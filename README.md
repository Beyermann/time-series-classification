# fml-project

## topological analysis

1. make visibility graphs python module


        $ cd visibilty_graphs
        $ make


1. create visibility graphs with create_vgs.py

        $ visibility_graphs/create_vgs.py --file <in_file> --output <out_dir>

1. build persistence diagrams from graph files

        $ bins/network_analysis_landscape --output <out_dir> <in_file>

1. get distances of diagrams

        $ bins/topological_distance [--hausdorf, --wasserstein, --landscape] \
                        <diagrams_dir>/*d[0,1,...]* > distances_d[0,1,...].txt

1. plot the resulting matrices
