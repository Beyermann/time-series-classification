#!/usr/bin/env python2
import argparse
import visibility_graphs

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Create Visibility Graphs from Power Time Series")
    parser.add_argument(
            "--file", type=str, default="/home/j/Scientific Computing/FML/Project/fml-project/data/trace_base/signals/BeanToCupCoffeemaker/converted_dev_B7BE96_2011.08.02.csv.p1_t77000",
            help="File with power time series")
    parser.add_argument(
            "--output", type=str, default="/tmp",
            help="file output folder")
    args = parser.parse_args()

    if not args.output.endswith("/"):
        args.output = args.output + "/"

    print(args.output)
    visibility_graphs.create_nvgs_from_file(
            args.file,
            args.output)
