#include <boost/python.hpp>
#include <algorithm>
#include <vector>
#include <fstream>
#include <iostream>
#include <memory>
#include <limits>
#include "signals.hh"
#include "csv.hh"

namespace python = boost::python;
using Series = std::vector<unsigned long>;
using Edge   = std::tuple<unsigned int, unsigned int>;
using Graph  = std::vector<Edge>;
using Line   = std::vector<unsigned long>;

void write_nvg(
        std::shared_ptr<Graph> g, 
        const std::string& output_filename, 
        const std::string& input_filename, 
        const unsigned long time_start,
        const unsigned long time_stop) 
{
    std::ofstream file(output_filename);
    file << "# file: " << input_filename << std::endl;
    file << "# signal start time: " << time_start << std::endl;
    file << "# signal stop time: " << time_stop << std::endl;
    file << "# number of edges: " << (*g).size() << std::endl;

    for (Edge e : *g)
        file << std::get<0>(e) << ' ' << std::get<1>(e) << std::endl;

    file.close();
}

bool _i_sees_j(
        const Series& t, 
        const Series& x, 
        const unsigned int i, 
        const unsigned int j) 
{
    bool connected = true;
    for (unsigned int k = i+1; k < j; ++k) {
        double projected = double(x[i]) + (double(x[j]) - double(x[i]))*(double(t[k]) - double(t[i]))/(double(t[j]) - double(t[i]));
        if (double(x[k]) >= projected) {
            connected = false;
            break;
        }
    }
    return connected;
}

void _split(
        const Series& t, 
        const Series& x, 
        const unsigned int i_min, 
        const unsigned int i_max, 
        std::shared_ptr<Graph> g) 
{
    auto max_elem = std::max_element(x.begin() + i_min, x.begin() + i_max + 1);
    unsigned int j = std::distance(x.begin(), max_elem);
    for (unsigned int i = i_min; i < j; ++i) {
        if (_i_sees_j(t, x, i, j)) {
            (*g).push_back(std::make_tuple(i, j));
        }
    }
    for (unsigned int i = j+1; i <= i_max; ++i) {
        if (_i_sees_j(t, x, j, i)) {
            (*g).push_back(std::make_tuple(j, i));
        }
    }
    if (j > i_min+1) { _split(t, x, i_min, j-1, g); }
    if (j+1 < i_max) { _split(t, x, j+1, i_max, g); }
}

std::shared_ptr<Graph> create_nvg(
        const Series& t, 
        const Series& s) 
{
    std::shared_ptr<Graph> g = std::make_shared<Graph>();
    _split(t, s, 0, s.size(), g);
    return g;
}

void create_nvgs_from_file(
        const std::string input_filename,
        const std::string output_dir)
{
    Series t;
    Series s;

    std::ifstream file(input_filename);
    std::string line;
    for (unsigned int i = 0; i < 3; ++i) //remove headers
        std::getline(file, line);
    while (std::getline(file,line)) {
        std::stringstream lineStream(line);
        std::string token;
        std::getline(lineStream, token, ';');
        t.push_back(std::stoul(token));
        std::getline(lineStream, token, '\n');
        s.push_back(std::stoul(token));
    }

    std::shared_ptr<Graph> g = create_nvg(t, s);

    std::string output_filename = output_dir + input_filename.substr(input_filename.rfind('/')+1);
    write_nvg(g, output_filename, input_filename, t[0], t[t.size()-1]);
}

BOOST_PYTHON_MODULE(visibility_graphs) {
    python::def("create_nvgs_from_file", create_nvgs_from_file);
}
