#ifndef CSV_HH
#define CSV_HH

#include <string>
#include <sstream>

using Line = std::vector<unsigned long>;

Line getNextLineAndSplitIntoTokens(std::istream& str)
{
    Line result;
    std::string line;
    std::string token;
    if (!std::getline(str,line))
        return Line({0});
    std::stringstream lineStream(line);

    // time;power
    for (unsigned int i = 0; i < 2; ++i) {
        std::getline(lineStream, token, ';');
        result.push_back(std::stoul(token));
    }

    return result;
}

#endif
