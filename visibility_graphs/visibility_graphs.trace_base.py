#!/usr/bin/env python2

import os, argparse
from visibility_graphs import create_nvgs_from_file
from glob import glob

BASE_DIR = '../data/trace_base/'
SIGNAL_DIR = BASE_DIR + 'signals/'
GRAPH_DIR = BASE_DIR + 'visibility_graphs/'

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', default=SIGNAL_DIR,
                        help='input folder (path to directory with signals)')
    parser.add_argument('-o', '--output', default=GRAPH_DIR,
                        help='output folder (where visibility graphs should be written to)')
    parser.add_argument('-v', action='store_true',
                        help='verbose')
    args = parser.parse_args()
    SIGNAL_DIR, GRAPH_DIR = args.input, args.output

    data_dirs = glob(SIGNAL_DIR + '*')

    for d in data_dirs :
        files = glob(d + '/*')
        output_dir = GRAPH_DIR + d.split('/')[-1] + '/'
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
    
        for f in files :
            if args.v : print('Creating visibility graph for {} in {}'.format(f.split('/')[-1], output_dir))
            create_nvgs_from_file(f, output_dir)
