#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (C) 2018 Conrad Sachweh

"""NAME
        %(prog)s - <description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

AUTHOR
        Conrad Sachweh, conrad@csachweh.de
"""
TEMP_DIR="/tmp/compare/"
#--------- Classes, Functions, etc ---------------------------------------------
class Example(object):
    """This shows how the docu should be included in a class...
    """
    x=0

def examplefunc():
    """This shows how the docu should be included in a function...
    """
    pass
#-------------------------------------------------------------------------------
#    Main
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys, os
    import argparse
    import glob
    from scripts import convert_tracebase
    from main_pipeline import create_visibility_graphs, create_diagrams, get_distances, plot_distance_matrix, process_file
    import subprocess
    
    # Command line option parsing example
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='show more verbose output')

    parser.add_argument('dirs', nargs="+",
                        help='input directories (only select from one! directory possible)')
    parser.add_argument('--skip-conversion', action='store_true',
                        help='do not convert files, if the files are already converted in input directory')
    parser.add_argument('-o', '--output', default=TEMP_DIR,
                        help='output directory')
    parser.add_argument('--converted-output', default='/tmp/converted/',
                        help='output for converted files')

    args = parser.parse_args()

    script_output = subprocess.DEVNULL
    if args.verbose > 1:
        script_output = subprocess.STDOUT

    # handle output directory
    out_dir = args.output
    if not out_dir.endswith("/"):
        out_dir += "/"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    converted_dir = args.converted_output
    if not out_dir.endswith("/"):
        converted_dir += "/"
    if not os.path.exists(converted_dir):
        os.makedirs(converted_dir)
    
    dirs = args.dirs
    for i, d in enumerate(dirs):
        if not d.endswith("/"):
            dirs[i] = d+"/"
    if len(dirs) < 2:
        print("[ERROR] need at least 2 directories")
        sys.exit(1)
    devices = {x.split("/")[-2]: x for x in dirs}

    print("[INFO] got {} directories. {}".format(len(dirs), list(devices.keys())))

    if not args.skip_conversion:
        # convert files to format we can handle
        for device, folder in devices.items():
            print("[INFO] starting with {} -- {}".format(device, folder))
            folder_index = glob.glob("{}*".format(folder))
            for file in folder_index:
                device,filename = convert_tracebase.get_file_info(file)
                conv = convert_tracebase.convert_file(file, args.verbose)

                temp_out_dir = "{}{}/".format(converted_dir, device)
                if not os.path.exists(temp_out_dir):
                        os.makedirs(temp_out_dir)
                with open("{}converted_{}".format(temp_out_dir, filename), "w") as f:
                    for line in conv:
                        f.write("{}\n".format(line))
    else:
        converted_dir = dirs[0].rsplit("/", 2)[0] # little hack, all subdirs have to be in same dir
        print("[DEBUG] converted_dir:", converted_dir)

    # 
    handle_dirs = glob.glob("{}/*".format(converted_dir))
    for folder in handle_dirs:
        device = folder.split("/")[-1]
        store_dir = "{}{}/".format(out_dir, device)
        files = glob.glob("{}/*".format(folder))
        for full_filename in files:
            # step 1:
            # create the visibility graphs with with create_vgs.py
            # as the boost module only works with python2 until now we have to call
            # the wrapper script unfortunately

            signal_dir = store_dir + "signals/"
            ret = create_visibility_graphs(full_filename, signal_dir, args.verbose)
            # we now got all signals graphs in 'signal_dir'
            if args.verbose > 1:
                print("[DEBUG_{}] ret_vcg: {}".format(filename, ret))


            # step 2:
            # build persistence diagrams from graph files
            diagrams_dir = store_dir + "diagrams/"
            ret = create_diagrams(signal_dir, diagrams_dir, args.verbose)
            # we now got all diagrams in 'diagrams_dir'
            if args.verbose > 1:
                print("[DEBUG_{}] ret_diagrams: {}".format(filename, ret))

            # step 3:
            # get distance of diagrams
            matrices_dir = store_dir + "matrices/"
            matrices = get_distances(diagrams_dir, matrices_dir, 'indicator', args.verbose)

            # step 4:
            # plot the resulting matrices
            plots_dir = store_dir + "plots/"
            for dim, matrix in matrices.items():
                plot_distance_matrix(matrix, plots_dir+device+"_d"+dim)
