#!/usr/bin/env python2

import os, argparse
from extract_signals import extract_signals 
from glob import glob

BASE_DIR = '../data/trace_base/'
DATA_DIR = BASE_DIR + 'converted_data/'
SIGNAL_DIR = BASE_DIR + 'signals/'

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', default=DATA_DIR,
                        help='input folder (path to tracebase converted data)')
    parser.add_argument('-o', '--output', default=SIGNAL_DIR,
                        help='output folder (where signals should be written to)')
    parser.add_argument('--max_dropout', default=5000,
                        help='maximal allowed intermission of power series in [ms]')
    parser.add_argument('--min_series_length', default=10,
                        help='minimal length of signal (shorter signals are ignored)')
    parser.add_argument('--min_power', default=40000,
                        help='minimal power (signals below that threshold are ignored)')
    parser.add_argument('-v', action='store_true',
                        help='verbose')
    args = parser.parse_args()
    DATA_DIR, SIGNAL_DIR = args.input, args.output
    max_dropout, min_series_length, min_power = args.max_dropout, args.min_series_length, args.min_power

    data_dirs = glob(DATA_DIR + '*')

    for d in data_dirs :

        files = glob(d + '/*')
        output_dir = SIGNAL_DIR + d.split('/')[-1] + '/'

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
    
        for f in files :
            if args.v : print('Extracting signals for {} to {}'.format(f.split('/')[-1], output_dir))
            extract_signals(f, output_dir,
                            max_dropout,
                            min_series_length,
                            min_power)

        # remove empty directories
        if not os.listdir(output_dir):
            os.rmdir(output_dir)
