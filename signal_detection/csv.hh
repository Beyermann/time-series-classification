#ifndef CSV_HH
#define CSV_HH

#include <string>
#include <sstream>

using Line = std::vector<unsigned long>;
//using Line = std::tuple<unsigned long, std::array<unsigned int, 3>>;

Line getNextLineAndSplitIntoTokens(std::istream& str)
{
    Line result;
    std::string line;
    std::string token;
    if (!std::getline(str,line))
        return Line({0});
    std::stringstream lineStream(line);

    // add time
    std::getline(lineStream, token, ';');
    result.push_back(std::stoul(token));

    // skip energy and power
    std::getline(lineStream, token, ';');
    std::getline(lineStream, token, ';');

    // add power1, power2, power3
    for (unsigned int i = 0; i < 3; ++i) {
        std::getline(lineStream, token, ';');
        result.push_back(std::stoul(token));
    }

    return result;
}

#endif
