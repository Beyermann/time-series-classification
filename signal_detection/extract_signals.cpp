#include <boost/python.hpp>
#include <algorithm>
#include <vector>
#include <fstream>
#include <iostream>
#include <memory>
#include <limits>
#include "signals.hh"
#include "csv.hh"

namespace python = boost::python;
using Series = std::vector<unsigned long>;
using Line   = std::vector<unsigned long>;

void write_signal(
        const Series& t,
        const Series& s, 
        const Range& r,
        const std::string& input_filename,
        std::string output_filename) 
{
    output_filename += std::to_string(t[std::get<0>(r)]) + ".txt";
    std::ofstream file(output_filename);
    file << "# Signal from file: " << input_filename << std::endl;
    file << "# Start time: " << t[std::get<0>(r)] << std::endl;
    file << "# Stop time: " << t[std::get<1>(r)] << std::endl;
 
    for (unsigned int i = std::get<0>(r); i <= std::get<1>(r); ++i)
        file << t[i] << ';' << s[i] << std::endl;

    file.close();
}

void process_series(
        const Series& times,
        const Series& series,
        const unsigned int& min_series_length,
        const std::string& input_filename,
        std::string& output_filename) 
{
    if (series.size() >= min_series_length) {
        std::vector<Range> ranges = get_signals(series, min_series_length);
        for (auto r : ranges) {
            write_signal(times, series, r, input_filename, output_filename);
        }
    }
}

/*void check_phase_value(
        const unsigned long time_value,
        const unsigned long phase_value,
        Series& times,
        Series& power,
        const unsigned long& min_power,
        const unsigned int& min_series_length,
        const std::string& input_filename,
        std::string& output_filename)
{
    if (phase_value >= min_power) {
        times.push_back(time_value);
        power.push_back(phase_value);
    }
    else {
        process_series(times, power, min_series_length, input_filename, output_filename);
        power.clear();
        times.clear();
    }
}*/

void extract_signals(
        const std::string input_filename,
        const std::string output_dir,
        const unsigned int max_dropout,
        const unsigned int min_series_length,
        const unsigned long min_power)
{
    Series power1;
    Series power2;
    Series power3;
    Series times;

    std::string filename = input_filename.substr(input_filename.rfind('/')+1);

    std::string output_filename1 = output_dir+filename+".p1_t";
    std::string output_filename2 = output_dir+filename+".p2_t";
    std::string output_filename3 = output_dir+filename+".p3_t";

    // open file and remove header
    std::ifstream file(input_filename);
    std::string line;
    std::getline(file, line);

    Line line_old = getNextLineAndSplitIntoTokens(file);
    Line line_new = getNextLineAndSplitIntoTokens(file);

    while(line_new[0] != 0) {

        // while no dropout of meter: append values to series
        while ((line_new[0] != 0) &&
               (line_new[0] < line_old[0] + max_dropout))
        {
            times.push_back(line_new[0]);
            power1.push_back(line_new[1]);
            power2.push_back(line_new[2]);
            power3.push_back(line_new[3]);
                
            line_old = line_new;
            line_new = getNextLineAndSplitIntoTokens(file);
        }

        // if dropout: process and reset all series
        process_series(times, power1, min_series_length, input_filename, output_filename1);
        process_series(times, power2, min_series_length, input_filename, output_filename2);
        process_series(times, power3, min_series_length, input_filename, output_filename3);
        power1.clear();
        power2.clear();
        power3.clear();
        times.clear();

        line_old = line_new;
        line_new = getNextLineAndSplitIntoTokens(file);
    }

    file.close();
}

BOOST_PYTHON_MODULE(extract_signals) {
    python::def("extract_signals", extract_signals);
}
