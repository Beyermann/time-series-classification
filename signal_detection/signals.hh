#ifndef SIGNALS_HH
#define SIGNALS_HH

#include <iostream>
#include <cassert>

using Range  = std::array<unsigned long, 2>;
using Series = std::vector<unsigned long>;

std::vector<Range> get_signals(const Series& s, const unsigned int& min_series_length) {
    std::vector<Range> signals;
    unsigned int len = s.size();
    std::cout << "Parsing series of length " << len << '\n';
    for (unsigned int i = 0; i < len-2; ++i) {
        if (s[i+1] > s[i] + 40000) {
            unsigned int start = i;
            while ((i < len-2) && ((s[i+1] > s[start]*1.1) || (s[i+2] > s[start]*1.1))) {
                i += 1;
            }
            unsigned int stop = i + 1;

            if (stop - start > min_series_length) {
                std::cout << "Added range from " << start << " to " << stop << "\nRange is ";
                for (unsigned int i = start; i <= stop; ++i)
                    std::cout << s[i] << ' ';
                std::cout << '\n';
                signals.push_back({start, stop});
                if (stop == len) { std::cout << "stop == len\n"; }
                if (stop == len-1) { std::cout << "stop == len-1\n"; }
            }
            assert(stop < len);
        }
    }
    return signals;
}

std::vector<Range> get_signals(const Series& s, const unsigned int& min_series_length) {
    std::vector<Range> signals;
    unsigned int len = s.size();
    //std::cout << "Parsing series of length " << len << '\n';
    for (unsigned int i = 0; i < len-2; ++i) {
        if (s[i] >= 40000) {
            unsigned int start = i;
            while ((i < len-2) && (s[i] > 40000)) {
                i += 1;
            }
            unsigned int stop = i + 1;

            if (stop - start > min_series_length) {
                /*std::cout << "Added range from " << start << " to " << stop << "\nRange is ";
                for (unsigned int i = start; i <= stop; ++i)
                    std::cout << s[i] << ' ';
                std::cout << '\n';*/
                signals.push_back({start, stop});
                //if (stop == len) { std::cout << "stop == len\n"; }
                //if (stop == len-1) { std::cout << "stop == len-1\n"; }
            }
            assert(stop < len);
        }
    }
    return signals;
}

#endif
