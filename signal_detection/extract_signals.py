#!/usr/bin/env python2
import argparse
from extract_signals import extract_signals 

n_minute = 5 * 6
n_hour = n_minute * 60
n_day = n_hour * 24

header = "time;energy;power;power1;power2;power3;energyOut;\n"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Extract signals from power time series")
    parser.add_argument(
            "--file", type=str, default="test_signal.csv",
            help="File with power time series")
    parser.add_argument(
            "--output", type=str, default="./",
            help="output directory")
    parser.add_argument(
            "--max_dropout", type=int, default=5000, # =^ 5s
            help="Maximal value for dropout. If dropout is longer, series are cut.")
    parser.add_argument(
            "--min_power", type=int, default=40000, # =^ 40W
            help="Power threshold for signal")
    parser.add_argument(
            "--min_series_length", type=int, default=10, # =^ 20s
            help="Minimal length of signal")
    args = parser.parse_args()

    with open(args.file) as f:
        f_header = f.readline()
        assert(header == f_header)
    if not args.output.endswith("/"):
        args.output = args.output + "/"

    extract_signals(
            args.file,
            args.output,
            args.max_dropout,
            args.min_series_length,
            args.min_power)
