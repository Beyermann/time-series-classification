#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (C) 2018 Conrad Sachweh

"""NAME
        %(prog)s - <description>
    
SYNOPSIS
        %(prog)s [--help]
        
DESCRIPTION
        none
        
FILES  
        none
     
SEE ALSO
        nothing
        
DIAGNOSTICS
        none
        
BUGS    
        none
    
AUTHOR
        Conrad Sachweh, conrad@csachweh.de
"""
DATA_DIR="data/"
BIN_DIR="bins/"
SCRIPTS_DIR="scripts/"
VGS_DIR="visibility_graphs/"
TEMP_DIR="/tmp/fml/"
EXTRACTION_TIMEOUT=120

import threading
lock_processed = threading.Lock()
lock_vgs = threading.Lock()
#--------- Classes, Functions, etc ---------------------------------------------
def create_visibility_graphs(in_file, out_dir, verbose=0):
    import os, subprocess
    log_name = in_file.split("/")[-1]
    """
    file: full filename
    directory: output directory
    """
    if verbose:
        print("[INFO_{}] creating signals in {}".format(log_name, out_dir))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    script_output = subprocess.DEVNULL
    if verbose > 1:
        script_output = subprocess.STDOUT

    cmd = [VGS_DIR+"create_vgs.py", "--file", in_file, "--output", out_dir]
    try:
        signals_output = subprocess.check_output(cmd, shell=False, timeout=EXTRACTION_TIMEOUT, stderr=script_output)
    except subprocess.TimeoutExpired:
        print("[WARNING_{}] did not extract all graphs, lets continue anyway".format(log_name))
        return
    except subprocess.CalledProcessError as e:
        print("[ERROR_{}] {} --> did not extract all graphs, maybe partially".format(log_name, e))
        with open(TEMP_DIR+"vgs_failed.txt", "a") as f:
            lock_vgs.acquire()
            f.write(in_file+"\n")
            lock_vgs.release()
        return
    print("[INFO_{}] created all graphs".format(log_name))

    return signals_output

def create_diagrams(file_dir, out_dir, verbose=0):
    import os, subprocess, glob
    log_name = file_dir.split("/")[-3]
    if verbose:
        print("[INFO_{}] creating diagrams in {}".format(log_name, out_dir))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    script_output = subprocess.DEVNULL
    if verbose > 1:
        script_output = subprocess.STDOUT

        
    signal_files = glob.glob(file_dir+"*")
    outp = []
    for f in signal_files:
        cmd = [BIN_DIR+"network_analysis_landscape", "--output", out_dir[:-1], f] # remove trailing '/'
        diagrams_output = subprocess.check_output(cmd, shell=False, timeout=None, stderr=script_output)
        outp.append(diagrams_output)

    return outp

def get_distances(file_dir, out_dir,  measure='indicator', verbose=0):
    import os, glob, subprocess
    import numpy as np
    log_name = file_dir.split("/")[-3]
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    diagram_files = glob.glob(file_dir+"*")
    dimensions = set([x[-5] for x in diagram_files])
    print("[INFO_{}] got dimensions {}".format(log_name, dimensions))

    DISTANCE_MEASURE = {"hausdorf": "--hausdorf",
                        "wasserstein": "--wasserstein",
                        "landscape": "--landscape",
                        "indicator": "--indicator",
    }
    # select from available distance_measure
    measure = DISTANCE_MEASURE["indicator"]

    # evaluate every dimension individually
    matrices = {}
    for dim in [0,]: # specify wanted dimensions
        files = [x for x in diagram_files if int(x[-5])== dim] # getting all files with d{dim}
        if len(" ".join(files)) > 131072:
            # this is max in current kernel for command line input 'getconf ARG_MAX'/16-1
            # https://unix.stackexchange.com/a/120652
            print("[ERROR_{}] too many files for command line input: {}".format(log_name, len(files)))
            # --> limit file input and remove last incomplete filename
            files = (" ".join(files)[:130000]).split(" ")[:-1]
        if verbose:
            print("[DEBUG_{}] files with dim {}: {}".format(log_name, dim, len(files)))
        if len(files) < 2:
            print("[ERROR_{}] can't calculate distance of only {} files".format(log_name, len(files)))
            break
        if verbose:
            print("[INFO_{}] evaluations for dimension {}".format(log_name, dim))
        cmd = [BIN_DIR+"topological_distance", measure]
        cmd.extend(files)
        if verbose > 1:
            distance_matrix = subprocess.check_output(" ".join(cmd), shell=True) # hack works!
        else:
            distance_matrix = subprocess.check_output(" ".join(cmd), shell=True, stderr=subprocess.DEVNULL) # this is needed, handling of shell + stderr...
        if verbose > 2:
            print("[DEBUG_{}] distance matrix for dim {}:\n {}".format(log_name, dim, distance_matrix))

        string_matrix = distance_matrix.decode('utf8')
        matrix = string_to_matrix(string_matrix)
        np.savetxt("{}matrix_d{}.txt".format(out_dir, dim), matrix, delimiter=' ')

        matrices[str(dim)] = matrix

    return matrices

def string_to_matrix(inp):
    import numpy as np
    lines = inp.split("\n")
    matrix = np.array([l.split(" ") for l in lines if l])
    m = matrix.astype(np.float)

    return m

def plot_distance_matrix(matrix, targetfile=None, fileformat=".svg", show=False):
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    log_name = targetfile.split("/")[-3]
    plt.figure()
    plt.imshow(matrix)
    plt.colorbar()
    # TODO: put xaxis to top
    plt.axes().xaxis.tick_top()
    if targetfile:
        fullpath = targetfile + fileformat
        target_dir = targetfile.rsplit("/", 1)[0]+"/"
        import os
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)

        print("[INFO_{}] saving file to {}".format(log_name, fullpath))
        plt.savefig(fullpath)
    if show:
        plt.show()

def purge_data(directory, verbose=0):
    if len(directory.split("/")) < 2:
        print("[ERROR] can only remove subdirectories")
    else:
        import shutil
        try:
            shutil.rmtree(directory)
            print("[INFO] successfully removed {}".format(directory))
        except FileNotFoundError:
            print("[WARNING] could not remove {} because it's not there".format(directory))
    return

def process_file(full_filename, args):
    files_processed=open(TEMP_DIR+"processed.txt", "a")
    filename = full_filename.split("/")[-1]
    store_dir = TEMP_DIR + filename + "/"
    print("[INFO_{}] starting pipeline for {}, output will be in {}".format(filename, filename, store_dir))

    # step 1:
    # create the visibility graphs with with create_vgs.py
    # as the boost module only works with python2 until now we have to call
    # the wrapper script unfortunately

    signal_dir = store_dir + "signals/"
    if not args.skip_vg:
        ret = create_visibility_graphs(full_filename, signal_dir, args.verbose)
        # we now got all signals graphs in 'signal_dir'
        if args.verbose > 1:
            print("[DEBUG_{}] ret_vcg: {}".format(filename, ret))

    # step 2:
    # build persistence diagrams from graph files
    diagrams_dir = store_dir + "diagrams/"
    if not args.skip_diagrams:
        ret = create_diagrams(signal_dir, diagrams_dir, args.verbose)
        # we now got all diagrams in 'diagrams_dir'
        if args.verbose > 1:
            print("[DEBUG_{}] ret_diagrams: {}".format(filename, ret))

    # step 3:
    # get distance of diagrams
    matrices_dir = store_dir + "matrices/"
    if not args.skip_matrices:
        matrices = get_distances(diagrams_dir, matrices_dir, 'indicator', args.verbose)

    # step 4:
    # plot the resulting matrices
    plots_dir = store_dir + "plots/"
    if not args.skip_plots:
        for dim, matrix in matrices.items():
            plot_distance_matrix(matrix, plots_dir+filename+"_d"+dim)

    lock_processed.acquire()
    files_processed.write(full_filename+"\n")
    files_processed.flush()
    lock_processed.release()

    return

#-------------------------------------------------------------------------------
#    Main 
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys, os
    import argparse
    import glob
    import numpy as np
    from multiprocessing import cpu_count

    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='show more verbose output (-vv is even more verbose)')

    parser.add_argument('files', nargs='+',
                        help='input files (at least one is needed, feel free to use *')

    parser.add_argument('-d', '--output', default=TEMP_DIR,
                        help='output folder')
    parser.add_argument('--cpu-count', type=int, default=cpu_count(),
                        help='specify count of cpus to use')

    parser.add_argument('--purge', action='store_true',
                        help='clean output directory from created files before running')

    parser.add_argument('--debug', action='store_true',
                        help='set some debug parameters (timeout, cpu_count)')

    parser.add_argument('--skip-vg', action='store_true',
                        help='skip creating visibility graph')
    parser.add_argument('--skip-diagrams', action='store_true',
                        help='skip creating diagrams')
    parser.add_argument('--skip-matrices', action='store_true',
                        help='skip creating matrices')
    parser.add_argument('--skip-plots', action='store_true',
                        help='skip creating plots')

    args = parser.parse_args()

    if args.debug:
        EXTRACTION_TIMEOUT = 5 # set shorter timeout for faster debugging
        args.cpu_count = 1 # only single process running

    if not args.output.endswith("/"):
        args.output = args.output + "/" # add trailing slash
    TEMP_DIR = args.output
    if args.purge:
        purge_data(TEMP_DIR) # make sure this is after got TEMP_DIR

    import subprocess
    # we only want the output of the called scripts in verbose mode
    script_output = subprocess.DEVNULL
    if args.verbose > 1:
        script_output = subprocess.STDOUT

    files_to_process = args.files
    try:
        with open(TEMP_DIR+"processed.txt", "r") as f:
            already_done = f.read().splitlines()
        files_to_process = [x for x in files_to_process if x not in already_done]
        print("[INFO] found processed file, omitting {}".format(already_done))

    except FileNotFoundError:
        if not os.path.exists(TEMP_DIR):
            os.makedirs(TEMP_DIR)
        f = open(TEMP_DIR+"processed.txt", "w")
        f.close()
        pass

    import concurrent.futures
    with concurrent.futures.ProcessPoolExecutor(max_workers=args.cpu_count) as executor:
        # Start the load operations and mark each future with its URL
        jobs = {executor.submit(process_file, x, args): x for x in files_to_process}
        for future in concurrent.futures.as_completed(jobs):
            res = jobs[future]
            print("[INFO_{}] finished: {}".format(res.split("/")[-1], res))
            try:
                data = future.result()
                if data:
                    print("with exit code: {}\n".format(data))
            except Exception as e:
                print('\n{} generated an exception: {}\n'.format(res, e))
