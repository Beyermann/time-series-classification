#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle
from matplotlib import pyplot as plt
import argparse, time
import numpy as np

def plot_results(arr, x, xlabel, ylabel) :
    for d in range(arr.shape[1]-1) :
        y = np.mean(arr[:,d,:], axis=1)
        yerr = np.std(arr[:,d,:], axis=1)
        plt.errorbar(x, y, yerr=yerr, label=devices[d])
    y = np.mean(arr[:,-1,:], axis=1)
    yerr = np.std(arr[:,-1,:], axis=1)
    plt.errorbar(x, y, yerr=yerr, color='k', label='average')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--file', type=str, default='ksweep_ml1_ntest2_ntrain5_niter2.pickle',
                        help='range of k values for sweep')
    args = parser.parse_args()
    
    with open(args.file, "rb") as f :
        devices, k_range, prec, rec, f1, sup = pickle.load(f)

    plot_results(prec, k_range, 'k', 'precision') 
    plot_results(rec, k_range, 'k', 'recall') 
    plot_results(f1, k_range, 'k', 'f1-score')
