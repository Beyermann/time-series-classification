#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (C) 2018 Conrad Sachweh & Jens Beyermann

"""NAME
        %(prog)s - <description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

AUTHOR
        Conrad Sachweh, conrad@csachweh.de
"""

#--------- Classes, Functions, etc ---------------------------------------------
class PrettyTable(list):
    def __init__(self, maxRowWidth=20, skipSame=False): # attention, skipSame=True introduces major overhead
        super().__init__()
        self.maxRowWidth = maxRowWidth
        self.skipSame = skipSame

    def insert(self, line):
        sanitized_line = [str(x) for x in line]
        if not sanitized_line:
            sanitized_line = ["\n"]
        if not self.skipSame or args.verbose > 1:
            self.append(sanitized_line)
        else:
            if not line in self:
                self.append(sanitized_line)

    def out(self):
        rowWidths = self.__calcRowWidth__()
        for i,j in enumerate(rowWidths):
            if j > self.maxRowWidth:
                rowWidths[i] = self.maxRowWidth # defaults to 20
        
        rowmask = ["{:"+str(x)+"} " for x in rowWidths[:]]

        outTable = []
        for line in self:
            for i, row in enumerate(line[:-1]):
                outTable.append(rowmask[i].format(row.format(rowWidths[i])))
            outTable.append("{}\n".format(line[-1])) #last row without rowmask and +\n

        outTable[-1] = outTable[-1][:-1] # remove last \n in output
        return outTable

    def __str__(self):
        if self:
            return "".join(self.out())
        else:
            return "[WARNING] {} empty".format(self.__class__.__name__)

    def __calcRowWidth__(self):
        count = len(max(self, key=len))
        width = [0] * count

        for line in self:
            for i, j in enumerate(line):
                if len(j) > width[i]:
                    width[i] = len(j)

        return width


    def sortByRow(self):
        print("sortByRow not possible")
        raise NotImplemented


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_classification_distances(test_item_index, test_item, device, medoids, verbose = 0):
    best_dist = {}
    d = device
    if args.verbose > 2:
        print("[DEBUG] tes item index",test_item_index)
        print("[DEBUG] currently classifying object from device", d)
    for category, vals in medoids.items():
        if args.verbose > 2:
            print("[DEBUG_comparing to:", category)
        for met, medoid_list in vals.items():
            #print("[INFO] metric:", met)
            #print("[DEBUG] medoid_list:", medoid_list)
            for i in medoid_list:
                medoid_signal = train_signals[category][i]
                if met == "l1":
                    dist = L1_distance_matrix([medoid_signal, test_item])[0][1] # get only top-right value
                if met == "fastdtw":
                    dist = dtw_distance_matrix([medoid_signal, test_item])[0][1] # get only top-right value
                if met == "topological_d0":
                    test_diagram_file = test_diagrams_d0_files[device][test_item_index]
                    medoid_diagram_file = train_diagrams_d0_files[category][i]
                    # path to medoid diagram
                    # print("[DEBUG] medoid: ", medoid_diagram_file)
                    # path to test_item diagram
                    # print("[DEBUG] test item: ", test_diagram_file)
                    
                    if (test_diagram_file == medoid_diagram_file):
                        raise Exception("this is not allowed to happen")
                    
                    tmp_topo_matrix = topological_distance_matrix([medoid_diagram_file, test_diagram_file], args.topo_metric)
                    #print("[DEBUG] topo matrix", tmp_topo_matrix)
                    dist = tmp_topo_matrix[0][1]
                if met == "topological_d1":
                    test_diagram_file = test_diagrams_d1_files[device][test_item_index]
                    medoid_diagram_file = train_diagrams_d1_files[category][i]
                    # path to medoid diagram
                    #print("[DEBUG] medoid: ", medoid_diagram_file)
                    # path to test_item diagram
                    #print("[DEBUG] test item: ", test_diagram_file)
                    
                    if (test_diagram_file == medoid_diagram_file):
                        raise Exception("this is not allowed to happen")
                    
                    tmp_topo_matrix = topological_distance_matrix([medoid_diagram_file, test_diagram_file], args.topo_metric)
                    #print("[DEBUG] topo matrix", tmp_topo_matrix)
                    dist = tmp_topo_matrix[0][1]
                #print("[INFO] dist:", dist)
                if not best_dist.get(met):
                    best_dist[met] = {"toTrain": i,
                                      "dist": dist,
                                      "device": category
                    }
                else:
                    if args.verbose > 2:
                        print("[DEBUG] old dist:", best_dist[met]["dist"])
                    if best_dist[met]["dist"] > dist:
                        best_dist[met] = {"toTrain": i,
                                          "dist": dist,
                                          "device": category
                        }

        if args.verbose > 2:
            print("[DEBUG] Best_dist for", category, "is", best_dist)
    return best_dist


def classify(dev, args, mpr):
    print("[INFO] trying to classify all", dev, "from test data set.")
    count_devices = len(test_signals[dev])
    count_positive = {x: 0 for x in metrics}
    count_negative = {x: 0 for x in metrics}

    count_iterations = 0 # only debug
    print("DEBUG_first:", mpr)
    for test_item_index, test_item in test_signals[dev].items():
        ret = get_classification_distances(test_item_index, test_item, dev, medoids, args.verbose)
        count_iterations +=1
        if count_iterations > 10 and args.debug:
            break

        for met, vals in ret.items():
            proposed_device = vals.get("device")
            dist = vals.get("dist")

            mpr[proposed_device][met]["total_predicted"] += 1

            if proposed_device == dev:
                color = bcolors.OKGREEN
                count_positive[met] += 1
                mpr[dev][met]["tp"] += 1
            else:
                color = bcolors.WARNING
                count_negative[met] += 1
                mpr[proposed_device][met]["fp"] += 1
            
            if args.verbose:
                print(color + "[INFO] {} classified {} as {} with dist: {}\t {}".format(met, dev, proposed_device, dist, proposed_device == dev) + bcolors.ENDC)

    ret = {}
    for met in metrics:
        mpr[dev][met]["total_label"] = count_devices
        ret[met] = {"count": count_devices,
                    "p": count_positive[met],
                    "n": count_negative[met],
                    "accuracy": (count_positive[met]/count_devices)*100,
        }
        print("[{}_{}] From {} devices classified {} right, {} wrong, accuracy:\t {}%".format(dev, met, count_devices, count_positive[met], count_negative[met], (count_positive[met]/count_devices)*100))
    print("[DEBUG] pr:", mpr)
    return (ret, mpr)

#-------------------------------------------------------------------------------
#    Main
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import os
    import sys
    import argparse
    import numpy.random as rnd
    import numpy as np
    import concurrent.futures

    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir) ))

    from util import *

    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--medoids', type=int, default=3,
                        help='specify count of medoids to compare to')
    parser.add_argument('-c', '--cpu-count', type=int, default=1,
                        help='specify count of cpus to use')
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='show more verbose output')
    parser.add_argument('--debug', action='store_true',
                        help='limit calculations for faster debugging')
    parser.add_argument('--distance-signal-count', type=int, default=10,
                        help='specify count of signals for creating the distance matrices')
    parser.add_argument('--d1', action='store_true', default=False,
                        help='specify if dimension 1 shall be computed')
    parser.add_argument('--topo-metric', default='indicator',
                        help='using other metric for calculation of topology')
    args = parser.parse_args()

    import glob

    SELECTION_FOLDER = "../data/selection_tracebase/"
    k = args.medoids
    devices = ["Dishwasher", "Freezer", "Refrigerator", "Washingmachine", "WaterFountain", "WaterKettle"]
    metrics = ["fastdtw", "l1", "topological_d0"]

    create_pr = {x:{"tp": 0, "fp": 0, "total_predicted": 0, "total_label": 0} for x in metrics}
    pr = {x:create_pr for x in devices}

    if args.d1:
      metrics.append("topological_d1")
    signals = {}
    diagrams_d0 = {}
    diagrams_d1 = {}
    for d in devices:
        signals[d] = np.array(glob.glob("{}signals/{}/*".format(SELECTION_FOLDER, d)))
        diagrams_d0[d] = np.array(glob.glob("{}diagrams/{}/*d0.txt".format(SELECTION_FOLDER, d)))
        if args.d1:
          diagrams_d1[d] = np.array(glob.glob("{}diagrams/{}/*d1.txt".format(SELECTION_FOLDER, d)))
        #distance_matrix[d] = glob.glob(

    # train - test split
    train_split_ratio = 0.7

    train_signals_files = {}
    train_signals = {}
    train_diagrams_d0_files = {}
    train_diagrams_d1_files = {}

    test_signals_files = {}
    test_signals = {}
    test_diagrams_d0_files = {}
    test_diagrams_d1_files = {}
    
    dtw_matrix = {}
    l1_matrix = {}
    topo_matrix_d0 = {}
    topo_matrix_d1 = {}
    medoids = {}
    for d in devices:
        print("[INFO] starting distance_matrix calculation for {}.".format(d))
        n = len(signals[d])
        indices = rnd.choice(n-1, size=int(np.floor(train_split_ratio * n)) , replace=False)
        # print("[DEBUG] indices: ", indices)
        train_signals_files[d] = signals[d][indices]
        train_signals[d] = get_signals(train_signals_files[d])
        train_diagrams_d0_files[d] = diagrams_d0[d][indices]
        if args.d1:
          train_diagrams_d1_files[d] = diagrams_d1[d][indices]
        
        test_signals_files[d] = np.setdiff1d(signals[d], train_signals_files[d])
        test_signals[d] = get_signals(test_signals_files[d])
        if len(test_signals[d]) > 500: # arbitrary border
            test_signals[d] = dict(list(test_signals[d].items())[:500])
        test_diagrams_d0_files[d] = np.setdiff1d(diagrams_d0[d],train_diagrams_d0_files[d])
        if args.d1:
          test_diagrams_d1_files[d] = np.setdiff1d(diagrams_d1[d],train_diagrams_d1_files[d])

        # dtw is really slow, take less signals
        sel = list(train_signals[d].keys())[:args.distance_signal_count] # here took 10, make configurable
        sel_train_signals = {k:train_signals[d][k] for k in sel}
        sel_train_signals_files = {k:train_signals_files[d][k] for k in sel}
        sel_train_diagrams_d0_files = {k:train_diagrams_d0_files[d][k] for k in sel}
        if args.d1:
          sel_train_diagrams_d1_files = {k:train_diagrams_d1_files[d][k] for k in sel}

        dtw_matrix[d] = dtw_distance_matrix(sel_train_signals)
        l1_matrix[d] = L1_distance_matrix(sel_train_signals)
        BIN_DIR = '../bins/'
        topo_matrix_d0[d] = topological_distance_matrix(sel_train_diagrams_d0_files.values(), args.topo_metric)
        if args.d1:
          topo_matrix_d1[d] = topological_distance_matrix(sel_train_diagrams_d1_files.values(), args.topo_metric)
        
        # get medoids of the train set
        medoids[d] = {}
        medoids[d]["fastdtw"] = medoid(dtw_matrix[d], 3)
        medoids[d]["l1"] = medoid(l1_matrix[d], 3)
        medoids[d]["topological_d0"] = medoid(topo_matrix_d0[d], 3)
        if args.d1:
          medoids[d]["topological_d1"] = medoid(topo_matrix_d1[d], 3)

    # now classify some data
    returned_vals = {}
    
    with concurrent.futures.ProcessPoolExecutor(max_workers=args.cpu_count) as executor:
       # Start the load operations and mark each future with its URL
       jobs = {executor.submit(classify, x, args, pr): x for x in devices}
       for future in concurrent.futures.as_completed(jobs):
           res = jobs[future]
           print("[INFO_{}] finished: {}".format(res.split("/")[-1], res))
           try:
               data, temp_pr = future.result()
               for mdev in temp_pr.keys():
                   for mmet in temp_pr[mdev].keys():
                       for mm in temp_pr[mdev][mmet].keys():
                           pr[mdev][mmet][mm] += temp_pr[mdev][mmet][mm]
               if data:
                   print("with exit code: {}\n".format(data))
                   returned_vals[res] = data
           except Exception as e:
               print('\n{} generated an exception: {}\n'.format(res, e))

    ## single threaded (for debugging)
    # for x in devices:
    #   returned_vals[x] = classify(x,args)

    # write data to disk
    from time import time
    cur_time = int(time())
    fn = "classification_evaluation_{}_{}_m{}_tm{}".format(cur_time, args.distance_signal_count, args.medoids, args.topo_metric)


    # pretty print
    prettyTable = PrettyTable(maxRowWidth=25)
    header = ["metric", "test_signals", "accuracy (%)", "positive", "negative"]
    for device, vals in returned_vals.items():
        prettyTable.insert([])
        prettyTable.insert([device])
        prettyTable.insert(header)
        for met, counts in vals.items():
            entry = [met, counts.get("count"), round(counts.get("accuracy"), 2), counts.get("p"), counts.get("n")]
            prettyTable.insert(entry)

    print(prettyTable)
    if not args.debug:
        import pickle
        tfnp = "{}.pickle".format(fn)
        with open(tfnp, "wb") as fd:
            pickle.dump(returned_vals, fd, protocol=4)

        tfnpr = "{}.pr".format(fn)
        with open(tfnpr, "wb") as fd:
            pickle.dump(pr, fd, protocol=4)

        tfnt = "{}.txt".format(fn)
        with open(tfnt, "w") as fd:
            fd.write(" ".join(prettyTable.out()))
