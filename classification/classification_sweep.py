#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from classify import *
import argparse, time
import pickle

devices = ["Dishwasher", "Freezer", "Refrigerator", "Washingmachine", "WaterFountain", "WaterKettle"]

def parse_cr_str(cr, device) :
    pass

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--k-sweep', action='store_true',
                        help='perform parameter sweep for k')
    #parser.add_argument('--k-range', type=str, default='1:2:9',
    #                    help='range of k values for sweep')
    parser.add_argument('--train-sweep', action='store_true',
                        help='perform parameter sweep for number of training signals')
    #parser.add_argument('--train-range', type=str, default='10:10:100',
    #                    help='range of n_train values for sweep')

    parser.add_argument('--n_iter', type=int, default=2,
                        help='number of iterations per classification run')
    parser.add_argument('-k', type=int, default=5,
                        help='number of medoids')
    parser.add_argument('-c', '--cpu-count', type=int, default=2,
                        help='specify count of cpus to use')
    parser.add_argument('-p', action='store_true',
                        help='plot precision/recall/f1-score')
    parser.add_argument('-v', action='store_true',
                        help='show more verbose output')
    parser.add_argument('--n_train', type=int, default=5,
                        help='number of training signals')
    parser.add_argument('--n_test', type=int, default=2,
                        help='number of test signals')
    parser.add_argument('-m', default='l1',
                        help='metric')
    args = parser.parse_args()
    metric = args.m
    n_test, n_train = args.n_test, args.n_train
    v = args.v
    n_iter = args.n_iter
    n_dev = len(devices)


    t1_start = time.perf_counter()
    t2_start = time.process_time()


    if args.k_sweep :
        #kmin, kstep, kmax = [int(x) for x in args.k_range.split(':')]
        #if kmax > n_train :
        #    print('[WARNING] kmax > n_train!!! Reducing kmax to n_train.')
        #    kmax = n_train
        k_range = [1,3,5,7,10,15,30]
        n_k = len(k_range)
        #n_k = int((kmax-kmin)/kstep+1)
        prec = np.zeros((n_k, n_dev+1, n_iter))*(-1)
        rec = np.zeros((n_k, n_dev+1, n_iter))*(-1)
        f1 = np.zeros((n_k, n_dev+1, n_iter))*(-1)
        sup = np.zeros((n_k, n_dev+1, n_iter))*(-1)
        cms = []
        for n in range(len(k_range)) :
            k = k_range[n]
            cms_kn = []
            for i in range(args.n_iter) :
                print('[INFO] Evaluating k = {} ({}/{})'.format(k, i+1, args.n_iter))
                cr, cm = run_training_and_classification(metric, devices, n_test, k, n_train, v, args.cpu_count)
                print(cr, cm)
                cms_kn.append(cm)
                cr = [list(filter(None, x.split(' '))) for x in list(filter(None, cr.split('\n')))[1:]]
                for l in range(len(devices)) :
                    idx = [i for i in range(len(cr)) if cr[i][0] == devices[l]][0]
                    prec[n,l,i] = np.float(cr[idx][1])
                    rec[n,l,i] = np.float(cr[idx][2])
                    f1[n,l,i] = np.float(cr[idx][3])
                    sup[n,l,i] = np.float(cr[idx][4])
               
                idx = [i for i in range(len(cr)) if cr[i][0] == 'avg'][0]
                prec[n,-1,i] = cr[idx][3]
                rec[n,-1,i] = cr[idx][4]
                f1[n,-1,i] = cr[idx][5]
                sup[n,-1,i] = cr[idx][6]

            cms.append(cms_kn)

        file_name = 'ksweep_m{}_ntest{}_ntrain{}_niter{}.pickle'.format(
                    metric, n_test, n_train, n_iter)
        with open(file_name, "wb") as f :
            obj = (devices, k_range, prec, rec, f1, sup)
            pickle.dump(obj, f, protocol=4)

    if args.train_sweep :
        #kmin, kstep, kmax = [int(x) for x in args.k_range.split(':')]
        #if kmax > n_train :
        #    print('[WARNING] kmax > n_train!!! Reducing kmax to n_train.')
        #    kmax = n_train
        k = args.k
        t_range = [5,10,20,30,50,70,100]
        n_t = len(t_range)
        #n_k = int((kmax-kmin)/kstep+1)
        prec = np.zeros((n_t, n_dev+1, n_iter))*(-1)
        rec = np.zeros((n_t, n_dev+1, n_iter))*(-1)
        f1 = np.zeros((n_t, n_dev+1, n_iter))*(-1)
        sup = np.zeros((n_t, n_dev+1, n_iter))*(-1)
        cms = []
        for n in range(len(t_range)) :
            n_train = t_range[n]
            cms_kn = []
            for i in range(args.n_iter) :
                print('[INFO] Evaluating n_train = {} ({}/{})'.format(n_train, i+1, args.n_iter))
                cr, cm = run_training_and_classification(metric, devices, n_test, k, n_train, v, args.cpu_count)
                print(cr, cm)
                cms_kn.append(cm)
                cr = [list(filter(None, x.split(' '))) for x in list(filter(None, cr.split('\n')))[1:]]
                for l in range(len(devices)) :
                    idx = [i for i in range(len(cr)) if cr[i][0] == devices[l]][0]
                    prec[n,l,i] = np.float(cr[idx][1])
                    rec[n,l,i] = np.float(cr[idx][2])
                    f1[n,l,i] = np.float(cr[idx][3])
                    sup[n,l,i] = np.float(cr[idx][4])
               
                idx = [i for i in range(len(cr)) if cr[i][0] == 'avg'][0]
                prec[n,-1,i] = cr[idx][3]
                rec[n,-1,i] = cr[idx][4]
                f1[n,-1,i] = cr[idx][5]
                sup[n,-1,i] = cr[idx][6]

            cms.append(cms_kn)

        file_name = 'trainsweep_m{}_ntest{}_ntrain{}_niter{}.pickle'.format(
                    metric, n_test, n_train, n_iter)
        with open(file_name, "wb") as f :
            obj = (devices, t_range, prec, rec, f1, sup)
            pickle.dump(obj, f, protocol=4)




    print("------------------------------------------------------------")
    t1_stop = time.perf_counter()
    t2_stop = time.process_time()
    print("[INFO] Elapsed time: %.1f [min]" % ((t1_stop-t1_start)/60))
    print("[INFO] CPU process time: %.1f [min]" % ((t2_stop-t2_start)/60))
    print("------------------------------------------------------------")
