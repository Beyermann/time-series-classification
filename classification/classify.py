#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import numpy as np
import concurrent.futures
import itertools
from glob import glob
from random import shuffle
from sklearn.metrics import confusion_matrix, classification_report

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir) ))
from util import *

metrics = ['l1', 'dtw', 'indicator0', 'indicator1', 'hausdorf0', 'hausdorf1']
devices = ["Dishwasher", "Freezer", "Refrigerator", "Washingmachine", "WaterFountain", "WaterKettle"]


SIGNAL_DIR = '../data/trace_base/signals/'
DIAGRAM_DIR = '../data/trace_base/diagrams/'
BIN_DIR = '../bins/'


#--------- Lots of wrappers for distance matrix computation ------------------------------------

def get_top_distance_matrix(files, measure) :
    import subprocess
    cmd = [BIN_DIR+"topological_distance", measure]
    cmd.extend(files)
    distance_matrix = subprocess.check_output(" ".join(cmd), shell=True, 
                                              stderr=subprocess.DEVNULL) 
    return string_to_matrix(distance_matrix.decode('utf8'))

def get_l1_distance_matrix(files) :
    signals = get_signals(files)
    return L1_distance_matrix(signals)

def get_dtw_distance_matrix(files) :
    signals = get_signals(files) 
    return dtw_distance_matrix(signals)

def get_hausdorf_distance_matrix(files) :
    return get_top_distance_matrix(files, '--hausdorf')

def get_indicator_distance_matrix(files) :
    return get_top_distance_matrix(files, '--indicator')

DISTANCE_MATRIX = {'l1' : get_l1_distance_matrix,
                   'dtw' : get_dtw_distance_matrix,
                   'hausdorf0' : get_hausdorf_distance_matrix,
                   'hausdorf1' : get_hausdorf_distance_matrix,
                   'indicator0' : get_indicator_distance_matrix,
                   'indicator1' : get_indicator_distance_matrix}

#--------- Classification helpers ------------------------------------

def get_k_medoids(files, metric, k) :
    distance_matrix = DISTANCE_MATRIX[metric](files)
    if k == 1:
        return [medoid(distance_matrix, k)]
    else :
        return medoid(distance_matrix, k)

def get_files_for_metric(device, metric) :
    if metric in ['l1', 'dtw'] :
        return glob(SIGNAL_DIR+device+'/*')
    else :
        return glob(DIAGRAM_DIR+device+'/*_d{}*'.format(metric[-1]))

def get_medoids_and_test_data(metric, devices, n, k, test_max) :
    medoids = {}
    test_data = {}
    for d in devices :
        # pick n files and determine k medoids
        files = get_files_for_metric(d, metric)
        shuffle(files)
        #print('Files for device {} :'.format(d))
        #for f in files : print('\t{}'.format(f))
        medoids_d = get_k_medoids(files[:n], metric, k)
        medoids_d = [files[i] for i in medoids_d]
        for m in medoids_d :
            medoids[m] = d
        # append rest to test_data
        for f in files[n:n+min(len(files),test_max)] :
            test_data[f] = d
        
    return medoids, test_data

def classify(test_file, medoids, metric) :
    best_dist = np.inf
    label = 'default'
    for m in medoids.keys() :
        dist = DISTANCE_MATRIX[metric]([test_file, m])[0][1]
        if dist < best_dist :
            best_dist = dist
            label = medoids[m]
    return label, test_file


def run_training_and_classification(metric, devices, n_test, k, n_train, v=True, cpu_count=1) :
    if v : print('[INFO] Computing medoids and test data')
    medoids, test_data = get_medoids_and_test_data(metric, devices, n_train, k, n_test) 

    labels_true = []
    labels_pred = []

    if v : print('[INFO] Classifying test data ({} files)'.format(len(test_data)))
    with concurrent.futures.ProcessPoolExecutor(max_workers=cpu_count) as executor:
        jobs = {executor.submit(classify, t, medoids, metric): t for t in list(test_data.keys())}
        for future in concurrent.futures.as_completed(jobs):
            res = jobs[future]
            try:
                label, tf = future.result()
                labels_pred.append(label)
                labels_true.append(test_data[tf])
            except Exception as e:
                print('\n{} generated an exception: {}\n'.format(res, e))

    cr = classification_report(labels_true, labels_pred, target_names=devices)
    cm = confusion_matrix(labels_true, labels_pred)
    return cr, cm

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-k', type=int, default=3,
                        help='specify count of medoids to compare to')
    parser.add_argument('-c', '--cpu-count', type=int, default=1,
                        help='specify count of cpus to use')
    parser.add_argument('-v', action='store_true',
                        help='show more verbose output')
    parser.add_argument('--n_train', type=int, default=10,
                        help='number of training signals')
    parser.add_argument('--n_test', type=int, default=10,
                        help='number of test signals')
    parser.add_argument('-m', default='l1',
                        help='metric')
    args = parser.parse_args()
    metric, n_test, k, n_train, v = args.m, args.n_test, args.k, args.n_train, args.v

    if args.m not in metrics :
        print('No known metric specifid. Possible choices are:', metrics)

    cr, cm = run_training_and_classification(metric, devices, n_test, k, n_train, v)

    print(cr)
    plot_confusion_matrix(cm, devices)
