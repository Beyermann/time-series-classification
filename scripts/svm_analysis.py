#!/usr/bin/env python
#
# topological_distance -i -k -p 1 PIF_????_d?.txt > Distances_PIF_1.txt

import numpy
import random
import sys

from sklearn import svm
from sklearn import model_selection

from sklearn.metrics         import accuracy_score
from sklearn.metrics         import precision_recall_curve
from sklearn.model_selection import cross_val_score

from sklearn.preprocessing import MinMaxScaler

K   = numpy.loadtxt(sys.argv[1])
K   = -K
C   = numpy.genfromtxt(sys.argv[2])
svm = svm.SVC(kernel='precomputed', C=4.25, probability=True, verbose=False)
n   = K.shape[0]
m   = len(set(C))

scaler = MinMaxScaler()
K      = scaler.fit_transform(K)

print("Processing (%d,%d)-matrix..." % (n,n), file=sys.stderr)

ss              = model_selection.ShuffleSplit(n_splits=20, test_size=0.20)
accuracy_scores = list()
roc_auc_scores  = list()

for train, test in ss.split(K):
  print("Using", len(train), "columns for training", file=sys.stderr)

  kTrain = K[train][:,train]
  cTrain = C[train]

  # For the test kernel matrix, the values between *all* training
  # vectors and all test vectors must be provided.
  kTest = K[test]
  kTest = kTest[:,train]

  cTest = C[test]
  svm.fit(kTrain, cTrain)
  prediction = svm.predict(kTest)
  accuracy   = accuracy_score(cTest, prediction)
  accuracy_scores.append(accuracy)

  # Only calculate precision--recall curves for two classes
  if m == 2:
    p,r,t = precision_recall_curve(cTest, svm.decision_function(kTest))
    for (x,y) in zip(r,p):
      print(x,y)

    print("\n")

print("Average accuracy:", sum(accuracy_scores)/len(accuracy_scores), file=sys.stderr)

scores = cross_val_score(svm, K, C, cv=50)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

sys.exit(0)

# TODO: document

values_C       = [ 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ]
parameter_grid = { 'C': values_C }
grid_search    = model_selection.GridSearchCV(svm, parameter_grid)

for train, test in ss.split(K):
  kTrain      = K[train][:,train]
  cTrain      = C[train]

  grid_search.fit(kTrain, cTrain)
  print(grid_search.best_score_)
  print(grid_search.best_params_)
