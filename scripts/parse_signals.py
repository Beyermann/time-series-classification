#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import sys
def parse_signal(filename):
  """ Parses the file specified in filename. 
  Returns the data_file of the signal beeing parsed,
  the start_time of the signal
  and the end_time of the signal.
  call with:
  data_file, start_time, end_time = parse_signal(signal_filename)"""
  f = open(filename)
  lines = f.readlines()[:4]
  data_file = lines[0].split(' ')[-1].rstrip('\n')
  start_time = lines[1].split(' ')[-1].rstrip('\n')
  end_time = lines[2].split(' ')[-1].rstrip('\n')
  phase = int(filename.split('/')[-1][1]) + 2
  return data_file, phase, np.uint64(start_time), np.uint64(end_time)

def process_line(file_object):
  row = file_object.readline().rstrip('\n').rstrip(';').split(';')
  row = np.array(row)
  #print("[DEBUG]:row ", row)
  row = row.astype(np.uint64)
  return row

def get_plot_data(data_file, phase, start_time, end_time):
  f = open(data_file)
  f.readline() # Skip first line, since its only header information
  row = process_line(f) #f.readline().rstrip('\n').split(';')
  lines = np.zeros((1,len(row)))
  
  # Throw away lines, until we found start_line.
  while(start_time > (row[0])):
    row = process_line(f) #f.readline().rstrip('\n').split(';')
    #print(row)

  # Add lines to storgae until we reach end_line.
  #print("[DEBUG]:row ", row)
  lines[:][0] = np.array(row)

  #print("[DEBUG]:lines ", lines)
  while(end_time > (row[0])):
    #print("[DEBUG]:lines ", lines.shape)
    row = process_line(f)#f.readline().rstrip('\n').split(';')
    #print("[DEBUG]:row ", row)
    lines = np.vstack((lines,row))
    #print("[DEBUG]:lines ", lines.shape)
  
  #print("[DEBUG]:lines ", lines)
  y = lines[:,phase]
  #print("shape:", lines.shape)
  #print(y)
  #print(lines[phase,:])
  x = lines[:,0]
  #lines[:][0]
  f.close()
  return x,y


def test():
  
  offset = 0.2
  #start_time = 1483969911080
  #end_time = 1483999390651
  #data_file = "/home/jens/Uni/discovergy_data/data/00001"
  
  signal_file = "/tmp/00001/signals/p2_t1483979479796"
  data_file, phase, start_time, end_time = parse_signal(signal_file)

  print("[Debug] data_file: ", data_file)
  print("[Debug] phase: ", phase)
  print("[Debug] start_time: ", start_time)
  print("[Debug] end_time: ", end_time)
  x,y = get_plot_data(data_file, phase, start_time, end_time)
  x_pre,y_pre = get_plot_data(data_file, phase, start_time-(offset*(end_time - start_time)),start_time)
  x_post,y_post = get_plot_data(data_file, phase, end_time, end_time+(offset*(end_time - start_time)))
  
  x_total = np.hstack((x_pre,x))
  x_total = np.hstack((x_total,x_post))

  y_total = np.hstack((y_pre,y))
  y_total = np.hstack((y_total,y_post))
  #print("[Debug] x: ", x)
  #print("[Debug] y: ", y)
  plt.plot(x_pre,y_pre,c='red')
  plt.plot(x,y)
  plt.plot(x_post,y_post,c='green')
  plt.show()

def main(argv):
  signal_file = argv[1]
  offset = float(argv[2])
  #offset = 0.2
  #start_time = 1483969911080
  #end_time = 1483999390651
  #data_file = "/home/jens/Uni/discovergy_data/data/00001"
  
  #signal_file = "/tmp/00001/signals/p2_t1483979479796"
  data_file, phase, start_time, end_time = parse_signal(signal_file)

  print("[Debug] data_file: ", data_file)
  print("[Debug] phase: ", phase)
  print("[Debug] start_time: ", start_time)
  print("[Debug] end_time: ", end_time)
  x,y = get_plot_data(data_file, phase, start_time, end_time)
  x_pre,y_pre = get_plot_data(data_file, phase, start_time-(offset*(end_time - start_time)),start_time)
  x_post,y_post = get_plot_data(data_file, phase, end_time, end_time+(offset*(end_time - start_time)))
  
  x_total = np.hstack((x_pre,x))
  x_total = np.hstack((x_total,x_post))

  y_total = np.hstack((y_pre,y))
  y_total = np.hstack((y_total,y_post))
  #print("[Debug] x: ", x)
  #print("[Debug] y: ", y)
  plt.plot(x_pre,y_pre,c='green')
  plt.plot(x,y)
  plt.plot(x_post,y_post,c='green')
  plt.show()

def usage ():
  print("Usage:",
  "\t./scripts/parse_signals SIGNALPATH OFFSETFACTOR",
  "\tPlots the signal in the file SIGNALPATH.",
  "\tOFFSETFACTOR specifies what percentage of the signals",
  "\tlength should be plottet before and after the signal.", sep="\n")
  return 

if __name__ == "__main__":
  if (len(sys.argv) == 3):
    main(sys.argv)
  else:
    usage()
