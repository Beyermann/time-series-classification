#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (C) 2018 Conrad Sachweh & Jens Beyermann

"""NAME
        %(prog)s - convert files from tracebase to our default format

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

AUTHOR
        Conrad Sachweh, conrad@csachweh.de
"""

#--------- Classes, Functions, etc ---------------------------------------------
def get_file_info(f):
    device = f.split("/")[-2]
    filename = f.split("/")[-1]
    return device, filename

def convert_file(f, verbose=1):
    from datetime import datetime

    device, filename = get_file_info(f)
    if verbose:
        print("[INFO] converting", device, filename)

    with open(f, "r") as f:
        try:
            d = f.readlines()
        except UnicodeDecodeError as e:
            print("[ERROR]", f,  e)
            d = f.read().split("\n")

    out = []
    out.append("time;energy;power;power1;power2;power3;energyOut;") # default header for our tool
    for i,line in enumerate(d):
        try:
            t = line.split(";")
            t_before = d[i-1].split(";")
            timestr = t[0]
            timestr_before = t_before[0]
            if timestr == timestr_before:
                consumption = (int(t[1]) + int(t_before[1]))*500
                last_line = out[-1]
                t_last = last_line.split(";")
                t_last[3] = consumption
                out[-1] = (";".join(str(x) for x in t_last))
            else:
                consumption = int(t[1])
                date = datetime.strptime(timestr, "%d/%m/%Y %H:%M:%S")
                l = [int(date.timestamp()*1000), 0, 0, int(consumption*1000), 0, 0, 0]
                out.append(";".join(str(x) for x in l))
        except:
            print("[ERROR] could not convert:", line)

    return out
#-------------------------------------------------------------------------------
#    Main
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys, os
    import argparse

    # Command line option parsing example
    parser = argparse.ArgumentParser()
    parser.add_argument('files', nargs="+", help='input file names')
    parser.add_argument('-o', '--output', default='converted/',
                        help='output directory')
    args = parser.parse_args()

    # handle output directory
    out_dir = args.output
    if not out_dir.endswith("/"):
        outdir += "/"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # convert files
    for f in args.files:
        device, filename = get_file_info(f)
        if not os.path.exists("{}{}/".format(out_dir, device)) : 
            os.makedirs("{}{}/".format(out_dir, device))
        out = convert_file(f)
        with open("{}{}/converted_{}".format(out_dir, device, filename), "w") as f:
            for line in out:
                f.write("{}\n".format(line))
