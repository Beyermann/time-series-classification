#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt

def distance_matrix(inputpath, targetfile=None):
    inputfile = open(inputpath,"r")
    inputfile = inputfile.readlines()
    matrix = np.array([line.strip("\n").split(" ") for line in inputfile if line[0] != "#" ])
    print(matrix)
    #matrix = matrix[:,0:-1]
    #print(type(matrix[0,0]))
    matrix = matrix.astype(np.float)
    print(matrix)
    plt.imshow(matrix)
    plt.colorbar()
    if (targetfile):
        plt.savefig(targetfile)
    plt.show()

if __name__ == "__main__":
  inputFile = sys.argv[1]
  if (len(sys.argv) == 3):
    outputFile = sys.argv[2]
    distance_matrix(inputFile, outputFile)
  else:
    distance_matrix(inputFile)
