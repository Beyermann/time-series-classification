#!/usr/bin/env python3

import os
import sys

def main(argv):
  inputFiles = argv[1:-1]
  outputDirectory = argv[-1]
  for f in inputFiles:
    cmd = "../bins/network_analysis_landscape --output " + outputDirectory + " " + f
    os.system(cmd)

if __name__ == "__main__":
  main(sys.argv)
