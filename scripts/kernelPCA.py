#!/usr/bin/env python

import sys
import numpy as np
import matplotlib.pyplot as plt

from sklearn.decomposition import KernelPCA
from sklearn import model_selection
from sklearn.preprocessing import KernelCenterer

from sklearn.metrics         import accuracy_score
from sklearn.metrics         import precision_recall_curve
from sklearn.model_selection import cross_val_score

from sklearn.preprocessing import MinMaxScaler

def main(arg_1, arg_2=None):
  """
  Input 
  """
  
  K   = np.loadtxt(arg_1)
  K   = -K
  C   = np.genfromtxt(arg_2)
  #C1 = np.ones(K.shape[0]//2)
  #C2 = -1 * np.ones(K.shape[0]//2)
  #print(C1,C2.dtype)
  #C = np.vstack((C1,C2))
  #print("shape of K: ",K.shape) 
  #print("counts of labels:\nC= 1", C[C==1].shape, "\nC=-1", C[C==-1].shape)
  kpca = KernelPCA(kernel = "precomputed") #svm.SVC(kernel='precomputed', C=4.25, probability=True, verbose=False)
  n   = K.shape[0]
  #m   = len(set(C))
  
  
  scaler = MinMaxScaler()
  K      = scaler.fit_transform(K)
  #print("Shape of K ",K.shape)
  #print("Diagonal of K: ",np.diag(K))

  K_kpca = kpca.fit_transform(K)
  #print("eigenvalues: \n total variance: ", np.sum(kpca.lambdas_), "\nc1+c2 variance: ",np.sum(kpca.lambdas_[:2]), "\npercentage: ", np.sum(kpca.lambdas_[:2])/np.sum(kpca.lambdas_))
  #print("Number of principle components: ", K_kpca.shape)
  covM = np.cov(K_kpca.T)
  #print("Shape of covariance matrix: ", covM.shape)
  #print("Covariance matrix: \n", covM[:,0])
  #print("Covariance matrix: \n", covM[:,1])
  #print("Covariance matrix: \n", covM[:,2])
  #print("Total variance: ", np.sum(np.diag(covM)))
  #print("First component variance: ", covM[0,0])
  #print("Second component variance: ", covM[1,1])
  #print("Portion of total variance: ", (covM[0,0] + covM[1,1]) / np.sum(np.diag(covM)) )
  
  print("# Total variance: ", np.sum(np.diag(covM)) , "\n",
        "# Component | Variance | Portion of Total\n")
  for i,var in enumerate(np.diag(covM)):
    print(i , covM[i,i], covM[i,i] / np.sum(np.diag(covM)))
  
  
  # create plots
  #plt.figure()
  t = 0.2
  l1 = 1
  l2 = 0
  
  #plt.subplot(2,3,1, aspect="equal")
  #plt.scatter(K_kpca[C==l2,0], K_kpca[C==l2,1], c="blue", alpha=t)
  #plt.scatter(K_kpca[C==l1,0], K_kpca[C==1,1], c="red", alpha=t)
  #plt.title("Projektion auf die ersten beiden Komponenten")
  #
  #plt.subplot(2,3,2, aspect="equal")
  #plt.scatter(K_kpca[C==l1,0], K_kpca[C==1,2], c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,0], K_kpca[C==l2,2], c="blue", alpha=t)
  #plt.title("comp 1 vs comp 3")
  #
  #plt.subplot(2,3,3, aspect="equal")
  #plt.scatter(K_kpca[C==l1,0], K_kpca[C==1,3], c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,0], K_kpca[C==l2,3], c="blue", alpha=t)
  #plt.title("comp 1 vs comp 4")
  #
  #plt.subplot(2,3,4, aspect="equal")
  #plt.scatter(K_kpca[C==l1,1], K_kpca[C==1,2], c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,1], K_kpca[C==l2,2], c="blue", alpha=t)
  #plt.title("comp 2 vs comp 3")
  #
  #plt.subplot(2,3,5, aspect="equal")
  #plt.scatter(K_kpca[C==l1,1], K_kpca[C==1,3], c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,1], K_kpca[C==l2,3], c="blue", alpha=t)
  #plt.title("comp 2 vs comp 4")
  #
  #plt.subplot(2,3,6, aspect="equal")
  #plt.scatter(K_kpca[C==l1,2], K_kpca[C==1,3], c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,2], K_kpca[C==l2,3], c="blue", alpha=t)
  #plt.title("comp 3 vs comp 4")
  #
  #plt.tight_layout()
  #plt.show()
  
  plt.figure()
  plt.scatter(K_kpca[C==l2,0], K_kpca[C==l2,1], c="blue", alpha=t)
  plt.scatter(K_kpca[C==l1,0], K_kpca[C==l1,1], c="red", alpha=t)
  plt.xlabel("Komponente 1")
  plt.ylabel("Komponente 2")
  plt.title("Projektion auf die ersten beiden Komponenten")
  #plt.show()
  
  count_l1 = len(C[C==l1])
  count_l2 = len(C[C==l2])
  
  cNumber = 10 # Number of components to plot 
  
  plt.figure()
  for k in range(cNumber):
    plt.scatter(K_kpca[C==l2,k], ((k+1) * np.ones(count_l2)) + 0.1, c="blue", alpha=t)
    plt.scatter(K_kpca[C==l1,k], ((k+1) * np.ones(count_l1)) - 0.1, c="red", alpha=t)
  #plt.scatter(K_kpca[C==1,0], 3 * np.ones(count_l1), c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,0], 3 * np.ones(count_l2), c="blue", alpha=t)
  #plt.scatter(K_kpca[C==1,1], 2 * np.ones(count_l1), c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,1], 2 * np.ones(count_l2), c="blue", alpha=t)
  #plt.scatter(K_kpca[C==1,2], 1 * np.ones(count_l1), c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,2], 1 * np.ones(count_l2), c="blue", alpha=t)
  #plt.scatter(K_kpca[C==1,0], 0 * np.ones(count_l1), c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,3], 0 * np.ones(count_l2), c="blue", alpha=t)
  plt.ylabel("component number")
  plt.xlabel("component value")
  plt.title("Distributions of single Components")
  plt.gca().invert_yaxis()
  plt.show()

  #plt.scatter(K_kpca[C==l1,0], K_kpca[C==1,1], c="red", alpha=t)
  #plt.scatter(K_kpca[C==l2,0], K_kpca[C==l2,1], c="blue", alpha=t)
  #plt.title("comp 1 vs comp 2")
  #plt.show()




  return

if __name__ == "__main__":
  #if (sys.argv[2]):
  main(sys.argv[1], sys.argv[2])
  #else:
  #main(sys.argv[1])
