import numpy as np
import pickle
from scipy.cluster.hierarchy import linkage
from scipy.cluster.hierarchy import dendrogram
from matplotlib import pyplot

def condensed_distance_matrix(distance_matrix) :
    """
    Converts the distance matrix to the format needed by the scipy linkage method.
    """
    d = distance_matrix.shape[0]
    pdist = np.zeros((int(d*(d-1)/2),))
    idx = 0
    for i in range(d) :
        for j in range(i+1,d) :
            pdist[idx] = distance_matrix[i, j]
            idx += 1
    return pdist

def plot_dendrogram(Z, ylim=None, title='test') :
    fig = pyplot.figure(title)
    dendrogram(Z,
               truncate_mode='lastp',
               leaf_rotation=90,
               leaf_font_size=10,
               count_sort=True)
    if ylim :
        ax = fig.gca()
        ax.set_ylim(0,ylim)

def get_clusters(Z, n_cluster=5, max_distance=None) :
    """
    Takes a linkage matrix Z and creates a list of clusters (which are itself
    lists of indices). The number of clusters can either be determined 
    explicitly by specifying a number n_cluster or implicitly by specifying a 
    maximal distance max_distance.
    """
    if n_cluster and max_distance :
        print('Cannot specify n_cluster AND max_distance.')
    if n_cluster :
        def stop(clusters, n_cluster, Z, max_distance, idx) :
            return len(clusters) <= n_cluster
    elif max_distance :
        def stop(clusters, n_cluster, Z, max_distance, idx) :
            return Z[idx, 2] > max_distance
        
    clusters = {}
    n_signals = Z.shape[0] + 1
    for idx in range(n_signals) :
        clusters[idx] = [idx]
    for idx in range(Z.shape[0]) :
        if stop(clusters, n_cluster, Z, max_distance, idx) :
            break
        i, j = int(Z[idx,0]), int(Z[idx,1])
        clusters[n_signals+idx] = clusters[i] + clusters[j]
        del clusters[i], clusters[j]
    clusters = [clusters[key] for key in clusters] # convert dict to list because
                                                   # keys are now useless
    return clusters

def write_cluster_list(clusters, output_filename, delim=';') :
    """
    Writes clusters=[[1], [2,3]] as:
    1;
    2;3;
    """
    with open(output_filename, 'w') as file :
        file.write('# some header stuff ... \n')  # TODO
        for cluster in clusters :
            line =  ''
            for i in cluster :
                line = line + str(i) + delim
            line = line +'\n'
            file.write(line)

def write_cluster_table(clusters, output_filename) : 
    """
    Writes clusters=[[1],[2,3]] as:
    1:1
    2:2
    3:2
    """
    #TODO maybe sort, s.t. indices are increasing
    with open(output_filename, 'w') as file :
        file.write('# some header stuff ... \n')  # TODO
        for j in range(len(clusters)) :
            for i in clusters[j] :
                print(str(i) + ':' + str(j) + '\n')
                file.write(str(i) + ':' + str(j) + '\n')

if __name__ == '__main__':
    testdir = '/home/j/Scientific Computing/FML/Project/fml-project/teststuff/'
    d_matrix = pickle.load(open(testdir+'distance_matrix.p', 'rb' ))
    cd_matrix = condensed_distance_matrix(d_matrix)
    Z = linkage(cd_matrix)
    plot_dendrogram(Z, ylim=1e5)
    clusters = get_clusters(Z)
    write_cluster_list(clusters, testdir+'cluster_list.txt')
    write_cluster_table(clusters, testdir+'cluster_table.txt')