Note: Reupload from a University project.

Wir verwenden persistente Homologie um die Unterschiedlichkeit der Visibility Graphen festzustellen.

Schritt 1: 
  Topologische Merkmale der einzelnen Graphen berechnen, und in Form von 
  Persistenzdiagrammen im Verzeichnis PFAD abspeichern. Dazu ruft das 
  build_persistence_diagrams script eine von (derzeit) zwei binaries auf. 
  Diese Binaries wenden unterschiedliche Filtrationen beim Bestimmen der 
  topologischen merkmale an.

  ./build_persistence_diagrams .../Graphs/* PATH

Schritt 2:
  Die Distanz der einzelnen Persistenzdiagramme bestimmen. In dem Programm topological_distance sind vier verschiedene Distanzmaße eingebaut. 
  --hausdor: default
  --wasserstein: recht genau aber aufwendig
  --landscape: etwas weniger genau, dafür deultich schneller
  --indicator: deutlich weniger genau dafür richtig richtig flott
  
  Es gibt noch einige parameter dieses Schrittes, mit denen man herumspielen kann, die wichtigsten sind:
  --power=p: Legt fest welche p-norm der Distanzberechnung zugrunde liegt.
  --kernel: Berechnet einen Kernel (also ein Ähnlichkeitsmaß) anstatt einer Distanzmatrix, wichtig für Kernel-PCA/SVM.
  --clean: Entfernt vermeintlich unwichtige Punkte aus dem Persistenzdiagramm bevor es weiterverarbeitet wird. Achtung, das kann die Ergebnisse verfälschen.

  ./topological_distance [--hausdorf, --wasserstein, --landscape] Diagrams/*d[0,1].txt > distances_d[0,1].txt

Schritt 3:
  Visualisieren Durch distanzmatrix oder Kernel-PCA der Distanzmatrix. Potentiell Clustering z.B. durch nen K-means (nur landscapes) oder K-center - artiges.
  Das Kernel-PCA script habe ich auch hinzugefügt, allerdings möchte das im Moment auch gerne Labels haben. Das muss ich noch etwas generischer schreiben.
  Ich versuche mal mir etwas zu überlegen, wie man den Binaries in zukunft unterschiedliche Filtrationen übergeben kann, ohne dabei jedes mal neu zu kompilieren.
