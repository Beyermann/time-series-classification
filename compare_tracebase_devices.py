#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (C) 2018 Conrad Sachweh

"""NAME
        %(prog)s - <description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

AUTHOR
        Conrad Sachweh, conrad@csachweh.de
"""
BIN_DIR="bins/"
TEMP_DIR="/tmp/tracebase_compared/"
#--------- Classes, Functions, etc ---------------------------------------------
def get_distances(diagram_files, out_dir, measure='indicator', verbose=0):
    import numpy as np
    log_name = ""

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    dimensions = set([x[-5] for x in diagram_files])
    print("[INFO_{}] got dimensions {}".format(log_name, dimensions))

    DISTANCE_MEASURE = {"hausdorf": "--hausdorf",
                        "wasserstein": "--wasserstein",
                        "landscape": "--landscape",
                        "indicator": "--indicator",
    }
    # select from available distance_measure
    measure = DISTANCE_MEASURE["indicator"]

    # evaluate every dimension individually
    matrices = {}
    for dim in [0,]: # specify wanted dimensions
        files = [x for x in diagram_files if int(x[-5])== dim] # getting all files with d{dim}
        if len(" ".join(files)) > 131072:
            # this is max in current kernel for command line input 'getconf ARG_MAX'/16-1
            # https://unix.stackexchange.com/a/120652
            print("[ERROR_{}] too many files for command line input: {}".format(log_name, len(files)))
            # --> limit file input and remove last incomplete filename
            files = (" ".join(files)[:130000]).split(" ")[:-1]
        if verbose:
            print("[DEBUG_{}] files with dim {}: {}".format(log_name, dim, len(files)))
        if len(files) < 2:
            print("[ERROR_{}] can't calculate distance of only {} files".format(log_name, len(files)))
            break
        if verbose:
            print("[INFO_{}] evaluations for dimension {}".format(log_name, dim))
        cmd = [BIN_DIR+"topological_distance", measure]
        cmd.extend(files)
        if verbose > 1:
            distance_matrix = subprocess.check_output(" ".join(cmd), shell=True) # hack works!
        else:
            distance_matrix = subprocess.check_output(" ".join(cmd), shell=True, stderr=subprocess.DEVNULL) # this is needed, handling of shell + stderr...
        if verbose > 2:
            print("[DEBUG_{}] distance matrix for dim {}:\n {}".format(log_name, dim, distance_matrix))

        string_matrix = distance_matrix.decode('utf8')
        matrix = string_to_matrix(string_matrix)
        np.savetxt("{}matrix_d{}.txt".format(out_dir, dim), matrix, delimiter=' ')

        matrices[str(dim)] = matrix

    return matrices

#-------------------------------------------------------------------------------
#    Main
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys, os
    import argparse
    import glob
    import subprocess
    from main_pipeline import string_to_matrix, plot_distance_matrix
    
    # Command line option parsing example
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='show more verbose output')

    parser.add_argument('dirs', nargs="+",
                        help='input directories of converted tracebase data')

    parser.add_argument('-o', '--output', default=TEMP_DIR,
                        help='output directory')

    args = parser.parse_args()

    script_output = subprocess.DEVNULL
    if args.verbose > 1:
        script_output = subprocess.STDOUT

    # handle output directory
    out_dir = args.output
    if not out_dir.endswith("/"):
        out_dir += "/"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    dirs = args.dirs
    for i, d in enumerate(dirs):
        if not d.endswith("/"):
            dirs[i] = d+"/"
    if len(dirs) < 2:
        print("[ERROR] need at least 2 directories")
        sys.exit(1)
    devices = {x.split("/")[-2]: x for x in dirs}

    print("[INFO] got {} directories. {}".format(len(dirs), list(devices.keys())))

    all_files = {}
    for device, folder in devices.items():
        files = glob.glob("{}diagrams/*".format(folder))
        all_files[device] = files
                          
    min_len = min(map(len, all_files.values())) # get lenght of shortest list
    print("[INFO] shortest list is {}".format(min_len))
    import random
    handle_files = []
    # get equal count of files from all selected data
    for f in all_files.values():
        handle_files.extend(random.sample(f, min_len))

    store_dir = "{}{}/".format(out_dir, "_".join(list(devices.keys())))
    # from diagrams get distances
    # equal parts
    matrices_dir = store_dir + "matrices/"
    matrices = get_distances(handle_files, matrices_dir, 'indicator', args.verbose)

    plots_dir = store_dir + "plots/"
    for dim, matrix in matrices.items():
        plot_distance_matrix(matrix, plots_dir+"compare_" + "_".join(list(devices.keys())) + "_d"+dim)
