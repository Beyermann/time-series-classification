from glob import glob
import numpy as np
import re

def get_signals(files) :
    signals = {}
    for i in range(len(files)) :
        with open(files[i]) as f :
            while (f.readline()[0] == '#') : continue # remove header
            signal = []
            while True:
                line = f.readline().rstrip('\n').split(';')
                if line[0] == '' : break
                signal.append(np.uint64(line[1]))
            signals[i] = signal
    return signals

def string_to_matrix(inp):
    lines = inp.split("\n")
    matrix = np.array([l.split(" ") for l in lines if l])
    m = matrix.astype(np.float)
    return m

def read_matrix(file_name, filelist=False) :
    with open(file_name) as f :
        lines = f.readlines()
        mlines = [l for l in lines if l[0] != '#']
        matrix = np.array([l.split(' ') for l in mlines])
        if filelist :
            files = [l.split(' ') for l in lines if l[0] == '#' and len(l) > 10]
            files = [f[1].rstrip('\n') for f in files if len(f) == 2]
            return matrix.astype(np.float), files
        return matrix.astype(np.float)

def write_matrix(matrix, filename, files, medoid=None) :
    footer = '\n'
    for f in files :
        footer += f + '\n'
    if medoid :
        footer += 'medoid = ' + medoid
    np.savetxt(filename, matrix, delimiter=' ', footer=footer)

def dtw_distance_matrix_file_list(DTW_DIR) :
    dirs = glob(DTW_DIR + '*')
    file_list = []
    for d in dirs :
        file_list += glob(d+'/*')
    return file_list

def l1_distance_matrix_file_list(L1N_DIR) :
    dirs = glob(L1N_DIR + '*')
    file_list = []
    for d in dirs :
        file_list += glob(d+'/*')
    return file_list

def topological_distance_matrix_file_list(dim, measure, TOP_DIR) :
    dirs = glob(TOP_DIR + '*')
    file_list = []
    for d in dirs :
        for f in glob(d+'/*') :
            splitted = f.split('/')[-1].split('.')[0].split('_')
            if 'd{}'.format(dim) in splitted and measure in splitted :
                file_list.append(f)# += [x for x in glob(d+'/*') if int(x[-5])==dim]
    return file_list

def medoid(distance_matrix, k = 1) :
    integrated_distance = np.sum(distance_matrix, axis=0)
    if k == 1:
      return np.argmin(integrated_distance)
    else:
      return np.argsort(integrated_distance)[:k]

def get_medoid_file_name(matrix_file):
    with open(matrix_file) as f :
        medoid_file = f.readlines()[-1].split(' ')[-1].rstrip('\n')
    return medoid_file

def condensed_distance_matrix(distance_matrix):
    """
    Converts the distance matrix to the format needed by the scipy linkage method.
    """
    d = distance_matrix.shape[0]
    pdist = np.zeros((int(d*(d-1)/2),))
    idx = 0
    for i in range(d) :
        for j in range(i+1,d) :
            pdist[idx] = distance_matrix[i, j]
            idx += 1
    return pdist

### distance functions
# fastdtw
def dtw_distance_matrix(signals) :
    from fastdtw import fastdtw
    from scipy.spatial.distance import euclidean
    d = len(signals)
    distance_matrix = np.zeros((d,d))
    for i in range(d) :
        for j in range(i+1, d) :
            distance_matrix[i,j], _ = fastdtw(signals[i], signals[j], dist=euclidean)
            distance_matrix[j,i] = distance_matrix[i,j]
    return distance_matrix

# L1
def normalize(signals) :
    for i in range(len(signals)) :
        signal = signals[i]
        s_min, s_max = np.min(signal), np.max(signal)
        if s_max == s_min :
            #print('s_max == s_min!!!')
            signals[i] = signal/s_max
        else :
            signals[i] = (signal - s_min)/(s_max - s_min)
    return signals

def normed_L1_distance(s1, s2) :
    # swap names if s2 > s1, s.t. always s1 > s2
    if len(s2) > len(s1) :
        tmp = s1
        s1 = s2
        s2 = tmp

    len1, len2 = len(s1), len(s2)
    d = len1 - len2
    distances = []
    for lag in range(d+1) :
        distances.append(np.sum(np.abs(s1[lag:lag+len2] - s2)))
    return min(distances)/len2 # normalize, s.t. longer signals do not automatically have a greater distance

def L1_distance_matrix(signals) :
    signals = normalize(signals)
    d = len(signals)
    distance_matrix = np.zeros((d,d))
    for i in range(d) :
        for j in range(i+1, d) :
            distance_matrix[i,j] = normed_L1_distance(signals[i], signals[j])
            distance_matrix[j,i] = distance_matrix[i,j]
    return distance_matrix

# topological
BIN_DIR = '../bins/'
TMP_DIR = '/tmp/fml/'

DISTANCE_MEASURE = {"hausdorf": "--hausdorf",
                    "landscape": "--landscape",
                    "indicator": "--indicator"}

def topological_distance_matrix(files, measure) :
    import subprocess
    cmd = [BIN_DIR+"topological_distance", DISTANCE_MEASURE[measure]]
    cmd.extend(files)
    distance_matrix = subprocess.check_output(" ".join(cmd), shell=True, 
                                              stderr=subprocess.DEVNULL) 
    string_matrix = distance_matrix.decode('utf8')
    return string_to_matrix(string_matrix)
def integrated_distance(distance_matrix) :
    dim = distance_matrix.shape[0]
    dist = 0
    for i in range(dim) :
        for j in range(i+1,dim) :
            dist += distance_matrix[i,j]
    return dist

def average_distance(matrix_file, v=False) :
    if v : print('Computing average distance for '+matrix_file)
    m = read_matrix(matrix_file)
    return(integrated_distance(m)/(m.shape[0]*(m.shape[0] - 1)/2))

def write_array_as_latex_table(arr, output_filename, hline=False, xlabels=None, ylabels=None, cell_str=' {5:2f} ') :

    if xlabels and len(xlabels) != arr.shape[1] :
        print('Array has shape {}x{} but {} xlabels were provided.'.format(
              arr.shape[0], arr.shape[1], len(xlabels)))
    if ylabels and len(ylabels) != arr.shape[0] :
        print('Array has shape {}x{} but {} xlabels were provided.'.format(
              arr.shape[0], arr.shape[1], len(ylabels)))

    with open(output_filename, 'w') as f :
        if hline : f.write('\\hline\n')
        if xlabels :
            if ylabels : f.write(' &')
            for i in range(len(xlabels)-1) :
                f.write(' {} &'.format(xlabels[i]))
            f.write(' {} \\\\'.format(xlabels[-1]))
            if hline : f.write('\\hline')
            f.write('\n')
        for i in range(arr.shape[0]) :
            try :
                if ylabels : f.write(' {} &'.format(ylabels[i]))
            except :
                print(i, len(ylabels), arr.shape[0])
            for j in range(arr.shape[1]-1) :
                f.write(cell_str.format(arr[i,j])+'&')
            f.write(cell_str.format(arr[i,-1])+'\\\\')
            if hline : f.write('\\hline')
            f.write('\n')
