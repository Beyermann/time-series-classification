#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
This script computes the within-class and between-class distances on signals from 
the tracebase dataset for, so far, 3 different distance metrics: 
* A topological distance based on the visibility graphs of the signals
* A simple L1 distance of the power series
* The difference of the power series according to the fastdtw algorithm


Output:
    Ratio average between-class distance to average within-class distance for all metrics.


When running the script the first time, use the flag '-c' in order to create all signals and diagrams.

If the converted tracebase data is not in '../data/trace_base/converted_data', use '-i path/to/converted_tracebase/'.

If you want to have the output somewhere else than in '../data/trace_base/' use '-o desired/output/location/'.

For checking if everything works reduce the number of signals to be considered for each device to e.g. 10 by using '-n 10'.

"""

import os, os.path, subprocess, time, sys, argparse

sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from util import * 
import numpy as np
from glob import glob
from main_pipeline import purge_data


def get_data_file_name(complete_file_name) :
    f = complete_file_name.split('/')[-1]
    idxs = [pos for pos, char in enumerate(f) if char=='.']
    return f[:idxs[-1]]

def get_medoid_file_name(matrix_file) :
    with open(matrix_file) as f :
        medoid_file = f.readlines()[-1].split(' ')[-1].rstrip('\n')
    return medoid_file

def eval_metric(matrix_file_list, class_distance_matrix_file) :

    if not matrix_file_list:
        print('Got empty list!')
        return -1, -1
    integrated_within_class_distance = 0
    n_distances = 0
    for f in matrix_file_list :
        m = read_matrix(f)
        integrated_within_class_distance += integrated_distance(m)
        n_distances += m.shape[0]*(m.shape[0] - 1)/2
    average_within_class_distance = integrated_within_class_distance/n_distances

    m = read_matrix(class_distance_matrix_file)
    integrated_between_class_distance = integrated_distance(m)
    average_between_class_distance = integrated_between_class_distance/(m.shape[0]*(m.shape[0] - 1)/2)
    
    return average_between_class_distance/average_within_class_distance

def total_average_distance(metric_dir, n) :
    matrix_file_list = glob(metric_dir+'/*/*_n{}*.txt'.format(n))
    integrated_within_class_distance = 0
    n_distances = 0
    for f in matrix_file_list :
        m = read_matrix(f)
        integrated_within_class_distance += integrated_distance(m)
        n_distances += m.shape[0]*(m.shape[0] - 1)/2
    return integrated_within_class_distance/n_distances

def ratio_matrix(distance_matrix_dir, n) :
    #metrics = [x for x in glob(distance_matrix_dir+'*') if x[-4:] != '.txt']
    metrics = [distance_matrix_dir + 'l1',
               distance_matrix_dir + 'fastdtw',
               distance_matrix_dir + 'indicator0',
               distance_matrix_dir + 'indicator1',
               distance_matrix_dir + 'hausdorf0',
               distance_matrix_dir + 'hausdorf1',
               distance_matrix_dir + 'landscape0',
               #distance_matrix_dir + 'landscape1'
               ]

    devices = sorted([x for x in glob(metrics[0]+'/*') if x[-4:] != '.txt'])
    n_dev = len(devices)
    matrix = np.ones((n_dev+1, len(metrics)))*(-1)
    for i in range(len(metrics)) :

        between_class_matrix = glob(metrics[i]+'/*_n{}*.txt'.format(n))
        if len(between_class_matrix) != 1 :
            print('{} between class distance matrix found for metric {}!'.format(
                  len(between_class_matrix), metrics[i]))
            continue 
        matrix[0,i] = average_distance(between_class_matrix[0])

        devices = sorted([x for x in glob(metrics[i]+'/*') if x[-4:] != '.txt'])
        if len(devices) != n_dev:
            print('{} devices found, expected {}'.format(len(devices), matrix.shape[0]-1))
            continue 
        for j in range(n_dev) :
            matrix[j+1,i] = matrix[0,i]/average_distance(glob(devices[j]+'/*_n{}*.txt'.format(n))[0])

        matrix[0,i] /= total_average_distance(metrics[i], n)
    metrics = [m.split('/')[-1] for m in metrics]
    devices = [d.split('/')[-1] for d in devices]
    return matrix, metrics, devices


def get_distance_metric_info(matrix_file) :
    m = read_matrix(matrix_file)
    m = condensed_distance_matrix(m)
    mean, mstd, mmin, mmax = np.mean(m), np.std(m), np.min(m), np.max(m)
    return mstd/mean, mmin/mean, mmax/mean

def run_compute_distance_matrix(cmd, files, output_file) :
    #print('Computing distance matrix for {} files'.format(len(files)))
    pos = [i for i, c in enumerate(output_file) if c == '/'][-1]
    output_dir = output_file[:pos+1]
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    cmd += ['-o', output_file, '-f']
    cmd.extend(files)
    subprocess.run(cmd)

if __name__=='__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', default='../data/trace_base/converted_data/',
                        help='input folder (path to tracebase converted data)')
    parser.add_argument('-o', '--output', default='../data/trace_base/',
                        help='folder where signals, graphs, diagrams are or should be written to if -c is used.')
    parser.add_argument('-c', action='store_true',
                        help='create signals and diagrams')
    parser.add_argument('-v', action='store_true',
                        help='verbose')
    parser.add_argument('-g', action='store_true',
                        help='get signal files from existing distance matrix')
    parser.add_argument('-n', type=int, default=100,
                        help='number of signals to be considered at most for each device')
    parser.add_argument('-d', '--dim', type=int, default=0,
                        help='dimension for which topological distance is computed')
    parser.add_argument('-m', '--measure', type=str, default='indicator',
                        help='topological distance measure')
    parser.add_argument('-t', '--time', action='store_true', default=False,
                        help='use time dependent filtration')
    parser.add_argument('--skip-ex-all', action='store_true',
                        help='skip computing all distances for which if distance_matrix.txt is existing')
    parser.add_argument('--skip-ex-top', action='store_true',
                        help='skip computing all topological distances if distance_matrix.txt is existing')
    parser.add_argument('--skip-ex-l1', action='store_true',
                        help='skip computing all l1 distances if distance_matrix.txt is existing')
    parser.add_argument('--skip-ex-dtw', action='store_true',
                        help='skip computing all dtw distances if distance_matrix.txt is existing')
    args = parser.parse_args()


    if args.skip_ex_all :
        args.skip_ex_top = True
        args.skip_ex_l1  = True
        args.skip_ex_dtw = True
    if args.dim == 2 :
        dim = [0, 1]
    else :
        dim = [args.dim]
    if args.measure == 'all' :
        measure = ['indicator', 'landscape', 'hausdorf']
    else :
        measure = [args.measure]
    n_max = args.n


    DATA_DIR = args.input
    BASE_DIR = args.output
    SIGNAL_DIR = BASE_DIR + 'signals/'
    GRAPH_DIR = BASE_DIR + 'visibility_graphs/'
    DIAGRAM_DIR = BASE_DIR + 'diagrams/'
    MATRIX_DIR = BASE_DIR + 'distance_matrices/'
    L1N_DIR = MATRIX_DIR + 'l1/'
    TOP_DIR = MATRIX_DIR + 'topological/'
    DTW_DIR = MATRIX_DIR + 'fastdtw/'


    if args.c :
        # try compiling, just in case :P
        subprocess.run(['make'], cwd = '../signal_detection')
        subprocess.run(['make'], cwd = '../visibility_graphs')
        print("------------------------------------------------------------")
        print("[INFO] Generating signal files, visibility graphs and diagrams.")
        print("------------------------------------------------------------")
        print("[INFO] Purging existent data:")
        for d in [SIGNAL_DIR, GRAPH_DIR, DIAGRAM_DIR, MATRIX_DIR] :
            purge_data(d)
        print("[INFO] Extracting signals...")
        cmd = ['../signal_detection/extract_signals.trace_base.py', '-i', DATA_DIR, '-o', SIGNAL_DIR]
        subprocess.run(cmd+['-v'] if args.v else cmd)
        print("[INFO] Creating visibility graphs...")
        cmd = ['../visibility_graphs/visibility_graphs.trace_base.py', '-i', SIGNAL_DIR , '-o', GRAPH_DIR]
        subprocess.run(cmd+['-v'] if args.v else cmd)
        print("[INFO] Creating diagrams...")
        if args.time:
          cmd = ['./create_diagrams.py', '-i', GRAPH_DIR , '-o', DIAGRAM_DIR, '-t']
        else:
          cmd = ['./create_diagrams.py', '-i', GRAPH_DIR , '-o', DIAGRAM_DIR]
        subprocess.run(cmd+['-v'] if args.v else cmd)


    t1_start = time.perf_counter()
    t2_start = time.process_time()


    print("------------------------------------------------------------")
    print("[INFO] Computing within-class distance matrices.")
    print("------------------------------------------------------------")

    data_dirs = glob(SIGNAL_DIR + '*')
    n_dirs = len(data_dirs)

    for i in range(n_dirs) :

        device = data_dirs[i].split('/')[-1]
        if args.g :
            file_name = glob('{}l1/{}/*_n{}*'.format(MATRIX_DIR, device, n_max))
            if len(file_name) == 0 : continue
            _, signal_files = read_matrix(file_name[0], True)
        else :
            signal_files = glob('{}/*'.format(data_dirs[i]))
        progress = '_{} ({}/{})'.format(device, i+1, n_dirs)
        if len(signal_files) < 50 :
            print("[INFO{}] Less than 10 signals found. Skipping.".format(progress))
            continue
        elif len(signal_files) > n_max :
            print("[INFO{}] More than {} signals found. Selecting {}.".format(progress, n_max, n_max))
            signal_files = signal_files[:n_max]
        else :
            print("[INFO{}] {} signals found.".format(progress,len(signal_files)))

        for m in measure :
            for d in dim :
                matrix_name = MATRIX_DIR+'{}{}/{}/distance_matrix_n{}_d{}_{}.txt'.format(
                                          m, d, device, n_max, d, m)
                if not (args.skip_ex_top and os.path.isfile(matrix_name)) :
                    print("[INFO{}] Computing topological distance matrix ({} {}).".format(progress, m, d))
                    diagram_dir = DIAGRAM_DIR + device + '/'
                    diagram_files = ['{}{}_d{}.txt'.format(diagram_dir, f.split('/')[-1][:-4], d) 
                                     for f in signal_files]
                    cmd = ['./compute_topological_distance_matrix.py', '-m', m, '-d', str(d)]
                    run_compute_distance_matrix(cmd, diagram_files, matrix_name)

        matrix_name = L1N_DIR+device+'/distance_matrix_n{}.txt'.format(n_max)
        if not (args.skip_ex_l1 and os.path.isfile(matrix_name)) :
            print("[INFO{}] Computing L1 distance matrix.".format(progress))
            cmd = ['./compute_l1_distance_matrix.py']
            run_compute_distance_matrix(cmd, signal_files, matrix_name)

        matrix_name = DTW_DIR+device+'/distance_matrix_n{}.txt'.format(n_max)
        if not (args.skip_ex_dtw and os.path.isfile(matrix_name)) :
            print("[INFO{}] Computing fastdtw distance matrix.".format(progress))
            cmd = ['./compute_fastdtw_distance_matrix.py']
            run_compute_distance_matrix(cmd, signal_files, matrix_name)


    print("------------------------------------------------------------")
    print("[INFO] Computing between-class distance matrices.")
    print("------------------------------------------------------------")

    for m in measure :
        for d in dim :
            matrix_name = MATRIX_DIR+'{}{}/distance_matrix_n{}_d{}_{}.txt'.format(
                                      m, d, n_max, d, m)
            if not (args.skip_ex_top and os.path.isfile(matrix_name)) :
                print("[INFO] Computing topological between class distance matrix.")
                matrices = glob(MATRIX_DIR+'{}{}/*/distance_matrix_n{}_d{}_{}.txt'.format(
                                m, d, n_max, d, m))
                medoids = [get_medoid_file_name(f) for f in matrices]
                cmd = ['./compute_topological_distance_matrix.py', '-m', m, '-d', str(d)]
                run_compute_distance_matrix(cmd, medoids, matrix_name)

    matrix_name = L1N_DIR+'/distance_matrix_n{}.txt'.format(n_max)
    if not (args.skip_ex_l1 and os.path.isfile(matrix_name)) :
        print("[INFO] Computing l1 between class distance matrix.")
        medoids = [get_medoid_file_name(f) for f in glob(L1N_DIR+'*/distance_matrix_n{}.txt'.format(n_max))]
        cmd = ['./compute_l1_distance_matrix.py']
        run_compute_distance_matrix(cmd, medoids, matrix_name)

    matrix_name = DTW_DIR+'/distance_matrix_n{}.txt'.format(n_max)
    if not (args.skip_ex_dtw and os.path.isfile(DTW_DIR+'/distance_matrix_n{}.txt'.format(n_max))) :
        print("[INFO] Computing fastdtw between class distance matrix.")
        medoids = [get_medoid_file_name(f) for f in glob(DTW_DIR+'*/distance_matrix_n{}.txt'.format(n_max))]
        cmd = ['./compute_fastdtw_distance_matrix.py']
        run_compute_distance_matrix(cmd, medoids, matrix_name)


    print("------------------------------------------------------------")
    print("[EVAL] Average between-class to within-class distance ratio:")
    matrix, metrics, devices = ratio_matrix(MATRIX_DIR, n_max)
    line = '{:>20}|'.format('')
    for m in metrics :
        line += ' {:>10} |'.format(m)
    print(line)
    line = '{:>20}|'.format('all')
    for j in range(matrix.shape[1]) :
        line += ' {:10.2f} |'.format(matrix[0,j])
    print(line)
    for i in range(1,matrix.shape[0]) :
        line = '{:>20}|'.format(devices[i-1])
        for j in range(matrix.shape[1]) :
            line += ' {:10.2f} |'.format(matrix[i,j])
        print(line)
    
    ylabels = ['all'] + devices
    write_array_as_latex_table(matrix, 'ratios.txt', hline=True, xlabels=metrics, ylabels=ylabels, cell_str=' {:5.2f} ')
#    r_top = eval_metric(topological_distance_matrix_file_list(d, m, TOP_DIR), 
#                        TOP_DIR+'distance_matrix_d{}_{}.txt'.format(d, m)) 
#    r_l1  = eval_metric(l1_distance_matrix_file_list(L1N_DIR), 
#                        L1N_DIR+'distance_matrix.txt')
#    r_dtw = eval_metric(dtw_distance_matrix_file_list(DTW_DIR), 
#                        DTW_DIR+'distance_matrix.txt')

#    print("[EVAL] Topology: {} (d:{}, m:{})".format(r_top, d, m))
#    print("[EVAL] L1 Norm:  {}".format(r_l1))
#    print("[EVAL] FastDTW:  {}".format(r_dtw))

#    print("[EVAL] STD, minimum and maximum relative to mean, for each class:")
#    devices = [d.split('/')[-1] for d in glob(L1N_DIR + '*') if d.split('.')[-1] != 'txt']
#    for i in range(len(devices)) :
#        stdrel, minrel, maxrel = get_distance_metric_info(TOP_DIR+devices[i]+'/distance_matrix_d{}_{}.txt'.format(dim, measure))
#        print("[EVAL_{}] Topology: {:5.2f} | {:5.2f} | {:5.2f}".format(devices[i], stdrel, minrel, maxrel))
#        stdrel, minrel, maxrel = get_distance_metric_info(L1N_DIR+devices[i]+'/distance_matrix.txt')
#        print("[EVAL_{}] L1 Norm:  {:5.2f} | {:5.2f} | {:5.2f}".format(devices[i], stdrel, minrel, maxrel))
#        stdrel, minrel, maxrel = get_distance_metric_info(DTW_DIR+devices[i]+'/distance_matrix.txt')
#        print("[EVAL_{}] FastDTW:  {:5.2f} | {:5.2f} | {:5.2f}".format(devices[i], stdrel, minrel, maxrel))
    

#    def get_n_files_and_distances(matrix_file, n) :
#        m, fs = read_matrix(matrix_file, True)
#        idxs = np.random.randint(0, len(fs), n)
#        n_fs = [fs[i] for i in idxs]
#        return n_fs

#    def print_matrix(m) :
#        for j in range(m.shape[0]) :
#            line = '|'
#            for k in range(m.shape[1]) :
#                line += ' {:10.2f} |'.format(m[j,k])
#                if k == n_signals -1 :
#                    line += '|'
#            print(line)
#            if j == n_signals - 1 :
#                line = '|'
#                for k in range(m.shape[1]) :
#                    line += '------------|'.format(m[j,k])
#                    if k == n_signals -1 :
#                        line += '|'

#                print(line)

#    n_signals = 5 
#    print("[EVAL] Distance matrix for signals from two random classes ({} signals each)".format(n_signals))
#    np.random.shuffle(devices)
#    out_dir = BASE_DIR + 'compare/'
#    for i in range(0, len(devices) - (len(devices) % 2), 2) :
#        print("[EVAL] {} vs {} :".format(devices[i], devices[i+1]))
#        print("       Topology :")
#        f1 = get_n_files_and_distances(TOP_DIR+devices[i]+'/distance_matrix_d{}_{}.txt'.format(dim, measure), n_signals)
#        f2 = get_n_files_and_distances(TOP_DIR+devices[i+1]+'/distance_matrix_d{}_{}.txt'.format(dim, measure), n_signals)
#        cmd = ['./compute_topological_distance_matrix.py', '-m', measure, '-d', str(dim)]
#        run_compute_distance_matrix(cmd, f1+f2, '{}{}_{}/'.format(out_dir,devices[i],devices[i+1]))
#        m = read_matrix('{}{}_{}/distance_matrix_d{}_{}.txt'.format(out_dir,devices[i],devices[i+1], dim, measure))
#        print(len(f1)+len(f2), len(f1+f2), m.shape)
#        print_matrix(m)

#        print("       L1 Norm :")
#        f1 = get_n_files_and_distances(L1N_DIR+devices[i]+'/distance_matrix.txt', n_signals)
#        f2 = get_n_files_and_distances(L1N_DIR+devices[i+1]+'/distance_matrix.txt', n_signals)
#        cmd = ['./compute_l1_distance_matrix.py']
#        run_compute_distance_matrix(cmd, f1+f2, '{}{}_{}/'.format(out_dir,devices[i],devices[i+1]))
#        m = read_matrix('{}{}_{}/distance_matrix.txt'.format(out_dir,devices[i],devices[i+1]))
#        print(len(f1)+len(f2), len(f1+f2), m.shape)
#        print_matrix(m)

                


    print("------------------------------------------------------------")
    t1_stop = time.perf_counter()
    t2_stop = time.process_time()
    print("[INFO] Elapsed time: %.1f [min]" % ((t1_stop-t1_start)/60))
    print("[INFO] CPU process time: %.1f [min]" % ((t2_stop-t2_start)/60))
    print("------------------------------------------------------------")
