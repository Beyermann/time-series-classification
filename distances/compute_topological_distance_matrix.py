#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

import subprocess, argparse
from util import *

BIN_DIR = '../bins/'
TMP_DIR = '/tmp/fml/'

DISTANCE_MEASURE = {"hausdorf": "--hausdorf",
                    "landscape": "--landscape",
                    "indicator": "--indicator"}

def topological_distance_matrix(files, measure) :
    cmd = [BIN_DIR+"topological_distance_with_unpaired_handling_for_indicator", DISTANCE_MEASURE[measure]]
    cmd.extend(files)
    distance_matrix = subprocess.check_output(" ".join(cmd), shell=True, 
                                              stderr=subprocess.DEVNULL) 
    string_matrix = distance_matrix.decode('utf8')
    return string_to_matrix(string_matrix)

if __name__=='__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--files', nargs='+',
                        help='input files (at least one is needed, feel free to use *')
    parser.add_argument('-o', '--output', default=TMP_DIR,
                        help='output file name')
    parser.add_argument('-d', '--dimension', type=int, default=0,
                        help='dimension for which the topological distance is computed')
    parser.add_argument('-m', '--measure', default="indicator",
                        help='measure which is used for the topological distance')
    args = parser.parse_args()
    files, output_file, dim, measure = args.files, args.output, args.dimension, args.measure

    matrix = topological_distance_matrix(files, measure)
    if len(files) != matrix.shape[0] :
        print("[ERROR] {} files were given but matrix has shape {}x{}!!!".format(
              len(files), matrix.shape[0], matrix.shape[1]))
    medoid_file = files[medoid(matrix)]
    write_matrix(matrix, output_file, files, medoid_file)
