#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, subprocess, argparse
from glob import glob

BIN_DIR = '../bins/'
BASE_DIR = '../data/trace_base/'
GRAPH_DIR = BASE_DIR + 'visibility_graphs/'
DIAGRAM_DIR = BASE_DIR + 'diagrams/'

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', default=GRAPH_DIR,
                        help='input folder (path to directory with visibility graphs)')
    parser.add_argument('-o', '--output', default=DIAGRAM_DIR,
                        help='output folder (where diagrams graphs should be written to)')
    parser.add_argument('-v', action='store_true',
                        help='verbose')
    parser.add_argument('-t', '--time', action='store_true',
                        help='specify if time dependent filtration is used')
    args = parser.parse_args()
    GRAPH_DIR, DIAGRAM_DIR = args.input, args.output

    data_dirs = glob(GRAPH_DIR + '*')
    
    for d in data_dirs :

        files = glob(d + '/*')
        output_dir = DIAGRAM_DIR + d.split('/')[-1] + '/'

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
    
        for f in files :
            if args.v : print('Creating diagram for {} in {}'.format(f.split('/')[-1], output_dir))
            if args.time: 
              cmd = [BIN_DIR+"network_analysis_time_dependent_filtration", "--output", output_dir[:-1], f]
            else:
              cmd = [BIN_DIR+"network_analysis_landscape", "--output", output_dir[:-1], f]
            output = subprocess.check_output(cmd, shell=False, timeout=None, stderr=subprocess.DEVNULL)
