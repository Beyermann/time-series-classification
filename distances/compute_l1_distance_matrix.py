#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

import argparse
from util import *

TMP_DIR = '/tmp/fml/'

    
if __name__=='__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--files', nargs='+',
                        help='input files (at least one is needed, feel free to use *')
    parser.add_argument('-o', '--output', default=TMP_DIR,
                        help='output file name')
    args = parser.parse_args()
    files, output_file = args.files, args.output
    
    signals = get_signals(files)
    matrix = L1_distance_matrix(signals)
    if len(files) != matrix.shape[0] :
        print("[ERROR] {} files were given but matrix has shape {}x{}!!!".format(
              len(files), matrix.shape[0], matrix.shape[1]))
    medoid_file = files[medoid(matrix)]
    write_matrix(matrix, output_file, files, medoid_file)
